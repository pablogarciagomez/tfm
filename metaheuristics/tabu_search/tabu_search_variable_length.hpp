/**
 * @file tabu_search_variable_length.hpp
 * @author Pablo
 * @brief Tabu search with a variable length tabu list.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef TABUSEARCHVARIABLELENGTH_HPP_
#define TABUSEARCHVARIABLELENGTH_HPP_

#include <metaheuristics/utils/move_data.hpp>
#include <metaheuristics/utils/tabu_list.hpp>

/**
 * @brief Provides static functions to do a tabu search with a variable length tabu list.
 * 
 * @tparam Filter enables filtering.
 */
template <typename Filter = std::false_type> class TabuSearchVariableLength
{
  public:
    /**
     * @brief Finds a solution to a problem using a tabu search metaheuristic with a variable length tabu list.
     * 
     * @tparam LocalSearchLogger type of the logger where a trace of the execution will be stored.
     * @tparam Solution type of the solution to be evaluated.
     * @tparam StoppingCriterion type of the stopping criterion to be used to terminate the algorithm.
     * @tparam Neighborhood type of the neighborhood to be used to find adjacent solutions.
     * @param logger logger where a trace of the execution will be stored.
     * @param initial_solution solution from where the search will start. 
     * @param min minimum size of the tabu list (the minimum size to which the list can shrink).
     * @param max maximum size of the tabu list (the maximum size to which the list can grow).
     * @param stopping_criterion stopping criterion to be used to terminate the algorithm.
     * @param neighborhood neighborhood to be used to find adjacent solutions.
     * @return the best solution found.
     */
    template <typename LocalSearchLogger, typename Solution, typename StoppingCriterion, typename Neighborhood>
    static Solution FindSolution(LocalSearchLogger& logger,
                                 const Solution& initial_solution,
                                 unsigned int min,
                                 unsigned int max,
                                 const StoppingCriterion& stopping_criterion,
                                 const Neighborhood& neighborhood)
    {
        using SolutionType = Solution;
        using MoveType = typename Neighborhood::MoveType;

        if (min == 0) {
            throw std::invalid_argument("min cannot be zero");
        }
        if (min >= max) {
            throw std::invalid_argument("max must be greater than min");
        }

        logger.AddLog(initial_solution.GetQuality(), 0, 0, "Initial solution");
        SolutionType current_solution = initial_solution; // the current solution
        SolutionType best_solution = current_solution; // the best found solution so far
        TabuList<MoveType> tabu_list(1); // the tabu list

        unsigned int iterations = 0; // number of iterations
        unsigned int no_improving_iterations = 0; // number of iterations without improving

        while (!stopping_criterion(iterations++, no_improving_iterations++)) {
            bool found_valid_neighbor = false;
            std::vector<MoveData<MoveType>> moves;
            neighborhood.GetNeighbors(std::inserter(moves, moves.begin()), current_solution);
            std::make_heap(moves.begin(), moves.end());
            auto total_neighbors = moves.size();
            unsigned int neighbors_evaluated = 0; // logging variable
            while (!moves.empty()) {
                std::pop_heap(moves.begin(), moves.end()); // moves the largest to the end
                auto move = moves.back();
                moves.pop_back(); // removes the largest element
                if constexpr (Filter::value) {
                    // if the neighbor is an estimate, calculate the exact value
                    // and reinsert in the priority queue
                    if (move.is_estimate) {
                        move.quality = current_solution.GetMoveQuality(move.move);
                        move.is_estimate = false;
                        moves.push_back(move);
                        std::push_heap(moves.begin(), moves.end());
                        continue;
                    }
                }
                neighbors_evaluated++;
                // aspiration criterion
                if (move.quality > best_solution.GetQuality()) {
                    if (!move.is_estimate || current_solution.GetMoveQuality(move.move) > best_solution.GetQuality()) {
                        current_solution.ApplyMove(move.move);
                        tabu_list.ForcePush(move.move.Invert());
                        tabu_list.ChangeCapacity(1);
                        best_solution = current_solution;
                        found_valid_neighbor = true;
                        no_improving_iterations = 0;
                        break;
                    }
                }
                // if the move is not tabu
                if (!tabu_list.Contains(move.move)) {
                    // establish the neighbor as the current solution
                    auto ancestor_quality = current_solution.GetQuality();
                    current_solution.ApplyMove(move.move);
                    tabu_list.ForcePush(move.move.Invert());
                    found_valid_neighbor = true;
                    // update the tabu list length
                    if (current_solution.GetQuality() > ancestor_quality) {
                        if (tabu_list.Capacity() > min) {
                            tabu_list.ChangeCapacity(tabu_list.Capacity() - 1);
                        }
                    } else {
                        if (tabu_list.Capacity() < max) {
                            tabu_list.ChangeCapacity(tabu_list.Capacity() + 1);
                        }
                    }
                    break;
                }
            }
            // if all moves are tabu reset
            if (!found_valid_neighbor) {
                if (moves.empty()) {
                    logger.AddLog(current_solution.GetQuality(), total_neighbors, neighbors_evaluated, "No neighbors available");
                    break;
                }
                tabu_list.ChangeCapacity(1);
            }
            logger.AddLog(current_solution.GetQuality(), total_neighbors, neighbors_evaluated, !found_valid_neighbor ? "All moves tabu" : "");
        }
        logger.AddLog(best_solution.GetQuality(), 0, 0, "Best solution");
        return best_solution;
    }
};

#endif /* TABUSEARCHVARIABLELENGTH_HPP_ */