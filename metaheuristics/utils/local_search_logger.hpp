/**
 * @file local_search_logger.hpp
 * @author Pablo
 * @brief Logger for local search algorithms.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef LOCALSEARCHLOGGER_HPP_
#define LOCALSEARCHLOGGER_HPP_

#include <string>
#include <vector>

/**
 * @brief Logger to trace the execution of a local search algorithm.
 * 
 * @tparam Active if true the logger will log the algorithm trace, if false the logger won't log the algorithm trace.
 */
template <typename Active = std::false_type> class LocalSearchLogger
{
  public:
    /**
     * @brief Structure that is used to log each step of the algorithm.
     * 
     */
    struct Log
    {
        double quality; // quality of the selected solution
        unsigned int neighbors_generated; // neighbors generated in the iteration
        unsigned int neighbors_evaluated; // neighbors evaluated in the iteration
        std::string msg; // optional message

        /**
         * @brief Constructs a new Log.
         * 
         * @param quality quality of the selected solution.
         * @param neighbors_generated neighbors generated in the iteration.
         * @param neighbors_evaluated neighbors evaluated in the iteration.
         * @param msg optional message.
         */
        Log(double quality, unsigned int neighbors_generated, unsigned int neighbors_evaluated, std::string msg = "") :
            quality{quality},
            neighbors_generated{neighbors_generated},
            neighbors_evaluated{neighbors_evaluated},
            msg{msg} {};

        friend std::ostream& operator<<(std::ostream& os, const Log& log)
        {
            if (log.msg != "") {
                os << log.msg << std::endl;
            }
            return os << "Neighbors generated = " << log.neighbors_generated << std::endl
                      << "Neighbors evaluated = " << log.neighbors_evaluated << std::endl
                      << "Quality = " << log.quality << std::endl;
        }
    };

  private:
    std::vector<Log> history;

  public:
    /**
     * @brief Constructs a new LocalSearchLogger.
     *  
     */
    LocalSearchLogger(){};

    /**
     * @brief Adds a log.
     * 
     * @param quality quality of the selected solution.
     * @param neighbors_generated neighbors generated in the iteration.
     * @param neighbors_evaluated neighbors evaluated in the iteration.
     * @param msg optional message.
     */
    void AddLog([[maybe_unused]] double quality,
                [[maybe_unused]] unsigned int neighbors_generated,
                [[maybe_unused]] unsigned int neighbors_evaluated,
                [[maybe_unused]] std::string msg = "")
    {
        if constexpr (Active::value) {
            history.emplace_back(quality, neighbors_generated, neighbors_evaluated, msg);
        }
    }

    /**
     * @brief Inserts in a container all the logged iterations.
     * 
     * @tparam Iter type of the iterator to be used to insert the logs.
     * @param dest iterator to be used to insert the logs.
     * @return an iterator to the log past the last log inserted. 
     */
    template <typename Iter> Iter GetHistory(Iter dest)
    {
        if constexpr (Active::value) {
            return std::copy(history.begin(), history.end(), dest);
        } else {
            return dest;
        }
    }

    /**
     * @brief Returns if the logger is active or not.
     * 
     * @return true if the logger is active. 
     * @return false if the logger is inactive.
     */
    constexpr explicit operator bool() const
    {
        return Active::value;
    }

    LocalSearchLogger& operator+=(const LocalSearchLogger& rhs)
    {
        if constexpr (Active::value) {
            history.insert(history.end(), rhs.history.begin(), rhs.history.end());
        }

        return *this;
    }

    friend LocalSearchLogger operator+(LocalSearchLogger lhs, const LocalSearchLogger& rhs)
    {
        if constexpr (Active::value) {
            lhs += rhs;
        }
        return lhs;
    }

    friend std::ostream& operator<<(std::ostream& os, const LocalSearchLogger& logger)
    {
        if (logger) {
            std::size_t i = 0;
            for (const auto& log: logger.history) {
                os << "Iteration " << i++ << std::endl << log;
            }
            return os;
        } else {
            return os;
        }
    }
};

#endif /* LOCALSEARCHLOGGER_HPP_ */