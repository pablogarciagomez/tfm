/**
 * @file move_data.hpp
 * @author Pablo
 * @brief Struture to communicate the problems with the local search algorithms.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef MOVEDATA_HPP_
#define MOVEDATA_HPP_

/**
 * @brief Auxiliary class that is used to pass a candidate move along with its estimated quality to the local search algorithms.
 * 
 * @tparam Move type of the move.
 */
template <typename Move> struct MoveData
{
    using MoveType = Move;

    MoveType move; // move that leads to a neighboring solution
    double quality; // quality of the neighboring solution
    bool is_estimate; // true if the quality is an estimate, false if it is an exact value

    /**
     * @brief Constructs a new MoveData.
     * 
     * @param move move that leads to a neighboring solution.
     * @param quality the quality of the neighboring solution. 
     * @param is_estimate true if the quality is an estimate, false if it is an exact value.
     */
    MoveData(const MoveType& move, double quality, bool is_estimate) : move{move}, quality{quality}, is_estimate{is_estimate} {};

    bool operator==(const MoveData& other) const
    {
        return move == other.move && quality == other.quality && is_estimate == other.is_estimate;
    };

    bool operator!=(const MoveData& other) const
    {
        return move != other.move || quality != other.quality || is_estimate == other.is_estimate;
    };

    bool operator<(const MoveData& other) const
    {
        return quality < other.quality;
    };

    bool operator>(const MoveData& other) const
    {
        return quality > other.quality;
    };

    bool operator<=(const MoveData& other) const
    {
        return quality <= other.quality;
    };

    bool operator>=(const MoveData& other) const
    {
        return quality >= other.quality;
    };
};

#endif /* MOVEDATA_HPP_ */
