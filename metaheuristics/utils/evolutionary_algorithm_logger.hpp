/**
 * @file evolutionary_algorithm_logger.hpp
 * @author Pablo
 * @brief Logger for evolutionary algorithms.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef EVOLUTIONARYALGORITHMLOGGER_HPP_
#define EVOLUTIONARYALGORITHMLOGGER_HPP_

#include <string>
#include <vector>

/**
 * @brief Logger to trace the execution of an evolutionary algorithm.
 * 
 * @tparam Active if true the logger will log the algorithm trace, if false the logger won't log the algorithm trace.
 */
template <typename Active = std::false_type> class EvolutionaryAlgorithmLogger
{
  public:
    /**
     * @brief Structure that is used to log each step of the algorithm.
     * 
     */
    struct Log
    {
        double average_quality; // average quality of the solutions
        double maximum_quality; // maximum quality of the solutions
        std::string msg; // optional message

        /**
         * @brief Constructs a new Log.
         * 
         * @param average_quality average quality of the solutions.
         * @param maximum_quality maximum quality of the solutions.
         * @param msg optional message.
         */
        Log(double average_quality, double maximum_quality, std::string msg = "") :
            average_quality{average_quality},
            maximum_quality{maximum_quality},
            msg{msg} {};

        friend std::ostream& operator<<(std::ostream& os, const Log& log)
        {
            if (log.msg != "") {
                os << log.msg << std::endl;
            }
            return os << "Average quality = " << log.average_quality << std::endl << "Maximum quality = " << log.maximum_quality << std::endl;
        }
    };

  private:
    std::vector<Log> history;

  public:
    /**
     * @brief Constructs a new EvolutionaryAlgorithmLogger.
     * 
     */
    EvolutionaryAlgorithmLogger(){};

    /**
     * @brief Adds a log.
     * 
     * @param average_quality average quality of the solutions.
     * @param maximum_quality maximum quality of the solutions.
     * @param msg optional message.
     */
    void AddLog([[maybe_unused]] double average_quality, [[maybe_unused]] double maximum_quality, [[maybe_unused]] std::string msg = "")
    {
        if constexpr (Active::value) {
            history.emplace_back(average_quality, maximum_quality, msg);
        }
    }

    /**
     * @brief Inserts in a container all the logged iterations.
     * 
     * @tparam Iter type of the iterator to be used to insert the logs.
     * @param dest iterator to be used to insert the logs.
     * @return an iterator to the log past the last log inserted. 
     */
    template <typename Iter> Iter GetHistory(Iter dest)
    {
        if constexpr (Active::value) {
            return std::copy(history.begin(), history.end(), dest);
        } else {
            return dest;
        }
    }

    /**
     * @brief Returns if the logger is active or not.
     * 
     * @return true if the logger is active. 
     * @return false if the logger is inactive.
     */
    constexpr explicit operator bool() const
    {
        return Active::value;
    }

    friend std::ostream& operator<<(std::ostream& os, const EvolutionaryAlgorithmLogger& logger)
    {
        if (logger) {
            std::size_t i = 0;
            for (const auto& log: logger.history) {
                os << "Iteration " << i++ << std::endl << log;
            }
            return os;
        } else {
            return os;
        }
    }
};

#endif /* EVOLUTIONARYALGORITHMLOGGER_HPP_ */