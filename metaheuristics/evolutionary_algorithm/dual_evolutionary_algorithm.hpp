/**
 * @file dual_evolutionary_algorithm.hpp
 * @author Pablo
 * @brief Dual Evolutionary Algorithm.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef DUALEVOLUTIONARYALGORITHM_HPP_
#define DUALEVOLUTIONARYALGORITHM_HPP_

#include <algorithm>
#include <numeric>
#include <random>
#include <unordered_set>

#include <utils/container_utils.hpp>

/**
 * @brief Provides static functions to run a dual evolutionary algorithm.
 * 
 */
class DualEvolutionaryAlgorithm
{
  private:
    /**
     * @brief Representation of an individual in a dual evolutionary algorithm.
     * 
     * @tparam GeneType1 type of the genes of the first chromosome.
     * @tparam GeneType2 type of the genes of the second chromosome.
     */
    template <typename GeneType1, typename GeneType2> struct Individual
    {
        using Chromosome1 = std::vector<GeneType1>;
        using Chromosome2 = std::vector<GeneType2>;

        Chromosome1 chromosome1; // the first chromosome of the individual
        Chromosome2 chromosome2; // the second chromosome of the individual
        double quality; // the quality (fitness) of the individual

        /**
         * @brief Returns the quality (fitness) of the individual.
         * 
         * @return the quality (fitness) of the individual. 
         */
        double GetQuality() const
        {
            return quality;
        }

        bool operator==(const Individual& other) const
        {
            return chromosome1 == other.chromosome1 && chromosome2 == other.chromosome2;
        }

        bool operator!=(const Individual& other) const
        {
            return chromosome1 != other.chromosome1 || chromosome2 != other.chromosome2;
        }

        bool operator<(const Individual& other) const
        {
            return quality < other.quality;
        };

        bool operator>(const Individual& other) const
        {
            return quality > other.quality;
        };

        bool operator<=(const Individual& other) const
        {
            return quality <= other.quality;
        };

        bool operator>=(const Individual& other) const
        {
            return quality >= other.quality;
        };
    };

    template <typename GeneType1, typename GeneType2> struct IndividualHash
    {
        inline std::size_t operator()(const Individual<GeneType1, GeneType2>& k) const
        {
            return hash_combine(vector_hash{}(k.chromosome1), vector_hash{}(k.chromosome2));
        }
    };

  public:
    /**
     * @brief Finds a solution to a problem using a dual evolutionary algorithm metaheuristic.
     * 
     * @tparam Solution type of the solution to be evaluated.
     * @tparam EvolutionaryAlgorithmLogger type of the logger where a trace of the evolutionary part of the execution will be stored.
     * @tparam Problem type of the problem to be evaluated.
     * @tparam EncoderDecoder type of the encoder/decoder to be used to evaluate the chromosomes.
     * @tparam GenerationOp type of the generation operator to be used to generate the initial population.
     * @tparam SelectionOp type of the selection operator to be used to choose the couples of individuals that will reproduce.
     * @tparam CrossoverOp1 type of the crossover operator to be used to cross the first chromosome.
     * @tparam CrossoverOp2 type of the crossover operator to be used to cross the second chromosome.
     * @tparam MutationOp1 type of the mutation operator to be used to mutate the first chromosome of the offsprings.
     * @tparam MutationOp2 type of the mutation operator to be used to mutate the second chromosome of the offsprings.
     * @tparam StoppingCriterion type of the stopping criterion to be used to terminate the algorithm.
     * @tparam ReplacementOp type of the replacement operator to be used to select the new generation.
     * @tparam RNG type of the random number generator.
     * @param logger logger where a trace of the execution will be stored.
     * @param problem problem to be solved.
     * @param encoder_decoder encoder/decoder to be used to evaluate the chromosomes.
     * @param generation_op generation operator to be used to generate the initial population.
     * @param population_size population size (number of individuals in the population).
     * @param selection_op selection operator to be used to choose the couples of individuals that will reproduce.
     * @param crossover_op1 crossover operator to be used to cross the first chromosome.
     * @param crossover_op2 crossover operator to be used to cross the second chromosome.
     * @param cross_prob cross probability.
     * @param mutation_op1 mutation operator to be used to mutate the first chromosome of the offsprings.
     * @param mutation_op2 mutation operator to be used to mutate the second chromosome of the offsprings.
     * @param mutation_prob mutation probability.
     * @param replacement_op replacement operator to be used to select the new generation.
     * @param elitism if true the best individual of each generation will pass untouched to the next generation.
     * @param stopping_criterion stopping criterion to be used to terminate the algorithm.
     * @param rng random number generator to be used.
     * @return the best solution found.
     */
    template <typename Solution,
              typename EvolutionaryAlgorithmLogger,
              typename Problem,
              typename EncoderDecoder,
              typename GenerationOp,
              typename SelectionOp,
              typename CrossoverOp1,
              typename CrossoverOp2,
              typename MutationOp1,
              typename MutationOp2,
              typename ReplacementOp,
              typename StoppingCriterion,
              typename RNG>
    static Solution FindSolution(EvolutionaryAlgorithmLogger& logger,
                                 const Problem& problem,
                                 const EncoderDecoder& encoder_decoder,
                                 const GenerationOp& generation_op,
                                 unsigned int population_size,
                                 const SelectionOp& selection_op,
                                 const CrossoverOp1& crossover_op1,
                                 const CrossoverOp2& crossover_op2,
                                 double cross_prob,
                                 const MutationOp1& mutation_op1,
                                 const MutationOp2& mutation_op2,
                                 double mutation_prob,
                                 const ReplacementOp& replacement_op,
                                 bool elitism,
                                 const StoppingCriterion& stopping_criterion,
                                 RNG& rng)
    {
        using SolutionType = Solution;
        using GeneType1 = typename EncoderDecoder::GeneType1;
        using GeneType2 = typename EncoderDecoder::GeneType2;
        using Individual = Individual<GeneType1, GeneType2>;
        using Population = std::unordered_multiset<Individual, IndividualHash<GeneType1, GeneType2>>;
        using Couple = std::pair<std::reference_wrapper<const Individual>, std::reference_wrapper<const Individual>>;

        // create the initial population
        Population population;
        {
            std::vector<SolutionType> raw_population;
            generation_op.template GetIndividuals<Solution>(std::back_inserter(raw_population), problem, population_size, rng);
            std::transform(std::make_move_iterator(raw_population.begin()),
                           std::make_move_iterator(raw_population.end()),
                           std::inserter(population, population.begin()),
                           [&encoder_decoder](const auto& i) {
                               Individual individual;
                               encoder_decoder.EncodeSolution(
                                   std::back_inserter(individual.chromosome1), std::back_inserter(individual.chromosome2), i);
                               individual.quality = i.GetQuality();
                               return individual;
                           });
        }

        // random number generator
        std::uniform_real_distribution<double> dis(0.0, 1.0);

        // keep the best solution
        Individual best_solution =
            *std::max_element(population.begin(), population.end(), [](const auto& t1, const auto& t2) { return t1.quality < t2.quality; });
        // number of generations
        unsigned int generations = 0;
        // number of generations without improving
        unsigned int no_improving_generations = 0;
        // average quality of the solutions
        double average_quality =
            std::accumulate(population.begin(), population.end(), 0.0, [](const auto& t1, const auto& t2) { return t1 + t2.quality; }) /
            population.size();
        logger.AddLog(average_quality, best_solution.quality);

        while (!stopping_criterion(generations++, no_improving_generations++, average_quality, best_solution.quality)) // termination criterion
        {
            Population new_generation;
            // elitism, the global best always pass to the next generation
            if (elitism) {
                new_generation.insert(best_solution);
            }
            // select the couples that will reproduce
            std::vector<Couple> couples;
            selection_op.Select(population.begin(), population.end(), std::back_insert_iterator(couples), population_size / 2, rng);
            for (const auto& [parent1, parent2]: couples) {
                // cross the individuals
                Individual offspring1;
                Individual offspring2;
                if (dis(rng) < cross_prob) {
                    crossover_op1.Cross(parent1.get().chromosome1.begin(),
                                        parent1.get().chromosome1.end(),
                                        parent2.get().chromosome1.begin(),
                                        parent2.get().chromosome1.end(),
                                        std::back_inserter(offspring1.chromosome1),
                                        std::back_inserter(offspring2.chromosome1),
                                        rng);
                    crossover_op2.Cross(parent1.get().chromosome2.begin(),
                                        parent1.get().chromosome2.end(),
                                        parent2.get().chromosome2.begin(),
                                        parent2.get().chromosome2.end(),
                                        std::back_inserter(offspring1.chromosome2),
                                        std::back_inserter(offspring2.chromosome2),
                                        rng);
                } else {
                    std::copy(parent1.get().chromosome1.begin(), parent1.get().chromosome1.end(), std::back_inserter(offspring1.chromosome1));
                    std::copy(parent1.get().chromosome2.begin(), parent1.get().chromosome2.end(), std::back_inserter(offspring1.chromosome2));
                    std::copy(parent2.get().chromosome1.begin(), parent2.get().chromosome1.end(), std::back_inserter(offspring2.chromosome1));
                    std::copy(parent2.get().chromosome2.begin(), parent2.get().chromosome2.end(), std::back_inserter(offspring2.chromosome2));
                }

                // mutate the offsprings
                if (dis(rng) < mutation_prob) {
                    mutation_op1.Mutate(offspring1.chromosome1.begin(), offspring1.chromosome1.end(), rng);
                    mutation_op2.Mutate(offspring1.chromosome2.begin(), offspring1.chromosome2.end(), rng);
                }
                if (dis(rng) < mutation_prob) {
                    mutation_op1.Mutate(offspring2.chromosome1.begin(), offspring2.chromosome1.end(), rng);
                    mutation_op2.Mutate(offspring2.chromosome2.begin(), offspring2.chromosome2.end(), rng);
                }

                // evaluate the fitness
                if (population.count(offspring1) != 0) {
                    offspring1.quality = population.find(offspring1)->quality;
                } else {
                    offspring1.quality = encoder_decoder.template EvaluateSolutionQuality<Solution>(offspring1.chromosome1.begin(),
                                                                                                    offspring1.chromosome1.end(),
                                                                                                    offspring1.chromosome2.begin(),
                                                                                                    offspring1.chromosome2.end(),
                                                                                                    problem);
                }
                if (population.count(offspring2) != 0) {
                    offspring2.quality = population.find(offspring2)->quality;
                } else {
                    offspring2.quality = encoder_decoder.template EvaluateSolutionQuality<Solution>(offspring2.chromosome1.begin(),
                                                                                                    offspring2.chromosome1.end(),
                                                                                                    offspring2.chromosome2.begin(),
                                                                                                    offspring2.chromosome2.end(),
                                                                                                    problem);
                }

                // select the individuals that will pass to the next generation
                const auto& [descendant1, descendant2] = replacement_op.Choose(parent1.get(), parent2.get(), offspring1, offspring2, rng);

                // insert the descendants in the new generation
                new_generation.insert(descendant1);
                new_generation.insert(descendant2);

                // check if any of the offsprings is the global best
                if (descendant1.get().quality > best_solution.quality) {
                    best_solution = descendant1;
                    no_improving_generations = 0;
                }
                if (descendant2.get().quality > best_solution.quality) {
                    best_solution = descendant2;
                    no_improving_generations = 0;
                }
            }
            // set the new generation as the current generation
            population = std::move(new_generation);
            // update average quality
            average_quality =
                std::accumulate(population.begin(), population.end(), 0.0, [](const auto& t1, const auto& t2) { return t1 + t2.quality; }) /
                population.size();
            logger.AddLog(average_quality, best_solution.quality);
        }

        // return the best solution found
        auto best = encoder_decoder.template DecodeSolution<Solution>(best_solution.chromosome1.begin(),
                                                                      best_solution.chromosome1.end(),
                                                                      best_solution.chromosome2.begin(),
                                                                      best_solution.chromosome2.end(),
                                                                      problem);
        logger.AddLog(average_quality, best_solution.quality, "Best solution");
        return best;
    }
};

#endif /* DUALEVOLUTIONARYALGORITHM_HPP_ */
