/**
 * @file memetic_algorithm.hpp
 * @author Pablo
 * @brief Memetic Algorithm.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef MEMETICALGORITHM_HPP_
#define MEMETICALGORITHM_HPP_

#include <algorithm>
#include <numeric>
#include <random>
#include <unordered_set>

#include <utils/container_utils.hpp>

/**
 * @brief Provides static functions to run a memetic algorithm.
 * 
 */
class MemeticAlgorithm
{
  private:
    /**
     * @brief Representation of an individual in a memetic algorithm.
     * 
     * @tparam GeneType type of the genes.
     */
    template <typename GeneType> struct Individual
    {
        using Chromosome = std::vector<GeneType>;

        Chromosome chromosome; // the chromosome of the individual
        double quality; // the quality (fitness) of the individual

        /**
         * @brief Returns the quality (fitness) of the individual.
         * 
         * @return the quality (fitness) of the individual. 
         */
        double GetQuality() const
        {
            return quality;
        }

        bool operator==(const Individual& other) const
        {
            return chromosome == other.chromosome;
        }

        bool operator!=(const Individual& other) const
        {
            return chromosome != other.chromosome;
        }

        bool operator<(const Individual& other) const
        {
            return quality < other.quality;
        };

        bool operator>(const Individual& other) const
        {
            return quality > other.quality;
        };

        bool operator<=(const Individual& other) const
        {
            return quality <= other.quality;
        };

        bool operator>=(const Individual& other) const
        {
            return quality >= other.quality;
        };
    };

    template <typename GeneType> struct IndividualHash
    {
        inline std::size_t operator()(const Individual<GeneType>& k) const
        {
            return vector_hash{}(k.chromosome);
        }
    };

  public:
    /**
     * @brief Finds a solution to a problem using a memetic algorithm metaheuristic.
     * 
     * @tparam Solution type of the solution to be evaluated.
     * @tparam EvolutionaryAlgorithmLogger type of the logger where a trace of the evolutionary part of the execution will be stored.
     * @tparam LocalSearchLogger type of the logger where a trace of the local parts of the execution will be stored.
     * @tparam Problem type of the problem to be evaluated.
     * @tparam EncoderDecoder type of the encoder/decoder to be used to evaluate the chromosomes.
     * @tparam GenerationOp type of the generation operator to be used to generate the initial population.
     * @tparam SelectionOp type of the selection operator to be used to choose the couples of individuals that will reproduce.
     * @tparam CrossoverOp type of the crossover operator to be used to cross the selected couples.
     * @tparam MutationOp type of the mutation operator to be used to mutate the chromosomes of the offsprings.
     * @tparam ReplacementOp type of the replacement operator to be used to select the new generation.
     * @tparam StoppingCriterion type of the stopping criterion to be used to terminate the algorithm.
     * @tparam RNG type of the random number generator.
     * @tparam LocalSearch type of the local search metaheuristic to be used to improve the individuals.
     * @tparam LocalSearchArgs type of the arguments of the local search metaheuristic.
     * @param evolutionary_logger logger where a trace of the evolutionary part of the execution will be stored.
     * @param local_logger logger where a trace of the local parts of the execution will be stored.
     * @param problem problem to be solved.
     * @param encoder_decoder encoder/decoder to be used to evaluate the chromosomes.
     * @param generation_op generation operator to be used to generate the initial population.
     * @param population_size population size (number of individuals in the population).
     * @param selection_op selection operator to be used to choose the couples of individuals that will reproduce.
     * @param crossover_op crossover operator to be used to cross the selected couples.
     * @param cross_prob cross probability.
     * @param mutation_op mutation operator to be used to mutate the chromosomes of the offsprings.
     * @param mutation_prob mutation probability.
     * @param replacement_op replacement operator to be used to select the new generation.
     * @param stopping_criterion stopping criterion to be used to terminate the algorithm.
     * @param elitism if true the best individual of each generation will pass untouched to the next generation.
     * @param rng random number generator to be used.
     * @param local_search local search metaheuristic to be used to improve the individuals.
     * @param local_search_prob improvement probability.
     * @param args arguments of the local search metaheuristic.
     * @return the best solution found. 
     */
    template <typename Solution,
              typename EvolutionaryAlgorithmLogger,
              typename LocalSearchLogger,
              typename Problem,
              typename EncoderDecoder,
              typename GenerationOp,
              typename SelectionOp,
              typename CrossoverOp,
              typename MutationOp,
              typename ReplacementOp,
              typename StoppingCriterion,
              typename RNG,
              typename LocalSearch,
              typename... LocalSearchArgs>
    static Solution FindSolution(EvolutionaryAlgorithmLogger& evolutionary_logger,
                                 LocalSearchLogger& local_logger,
                                 const Problem& problem,
                                 const EncoderDecoder& encoder_decoder,
                                 const GenerationOp& generation_op,
                                 unsigned int population_size,
                                 const SelectionOp& selection_op,
                                 const CrossoverOp& crossover_op,
                                 double cross_prob,
                                 const MutationOp& mutation_op,
                                 double mutation_prob,
                                 const ReplacementOp& replacement_op,
                                 bool elitism,
                                 const StoppingCriterion& stopping_criterion,
                                 RNG& rng,
                                 const LocalSearch& local_search,
                                 double local_search_prob,
                                 const LocalSearchArgs&... args)
    {
        using SolutionType = Solution;
        using GeneType = typename EncoderDecoder::GeneType;
        using Individual = Individual<GeneType>;
        using Population = std::unordered_multiset<Individual, IndividualHash<GeneType>>;
        using Couple = std::pair<std::reference_wrapper<const Individual>, std::reference_wrapper<const Individual>>;

        // create the initial population
        Population population;
        {
            std::vector<SolutionType> raw_population;
            generation_op.template GetIndividuals<Solution>(std::back_inserter(raw_population), problem, population_size, rng);
            std::transform(std::make_move_iterator(raw_population.begin()),
                           std::make_move_iterator(raw_population.end()),
                           std::inserter(population, population.begin()),
                           [&encoder_decoder](const auto& i) {
                               Individual individual;
                               encoder_decoder.EncodeSolution(std::back_inserter(individual.chromosome), i);
                               individual.quality = i.GetQuality();
                               return individual;
                           });
        }

        // random number generator
        std::uniform_real_distribution<double> dis(0.0, 1.0);

        // keep the best solution
        Individual best_solution =
            *std::max_element(population.begin(), population.end(), [](const auto& t1, const auto& t2) { return t1.quality < t2.quality; });
        // number of generations
        unsigned int generations = 0;
        // number of generations without improving
        unsigned int no_improving_generations = 0;
        // average quality of the solutions
        double average_quality =
            std::accumulate(population.begin(), population.end(), 0.0, [](const auto& t1, const auto& t2) { return t1 + t2.quality; }) /
            population.size();
        evolutionary_logger.AddLog(average_quality, best_solution.quality);

        while (!stopping_criterion(generations++, no_improving_generations++, average_quality, best_solution.quality)) // termination criterion
        {
            Population new_generation;
            // elitism, the global best always pass to the next generation
            if (elitism) {
                new_generation.insert(best_solution);
            }
            // select the couples that will reproduce
            std::vector<Couple> couples;
            selection_op.Select(population.begin(), population.end(), std::back_insert_iterator(couples), population_size / 2, rng);
#pragma omp parallel for schedule(guided)
            for (auto it = couples.begin(); it < couples.end(); it++) {
                const Individual& parent1 = it->first;
                const Individual& parent2 = it->second;
                // cross the individuals
                Individual offspring1;
                Individual offspring2;
                if (dis(rng) < cross_prob) {
                    crossover_op.Cross(parent1.chromosome.begin(),
                                       parent1.chromosome.end(),
                                       parent2.chromosome.begin(),
                                       parent2.chromosome.end(),
                                       std::back_inserter(offspring1.chromosome),
                                       std::back_inserter(offspring2.chromosome),
                                       rng);
                } else {
                    std::copy(parent1.chromosome.begin(), parent1.chromosome.end(), std::back_inserter(offspring1.chromosome));
                    std::copy(parent2.chromosome.begin(), parent2.chromosome.end(), std::back_inserter(offspring2.chromosome));
                }

                // mutate the offsprings
                if (dis(rng) < mutation_prob) {
                    mutation_op.Mutate(offspring1.chromosome.begin(), offspring1.chromosome.end(), rng);
                }
                if (dis(rng) < mutation_prob) {
                    mutation_op.Mutate(offspring2.chromosome.begin(), offspring2.chromosome.end(), rng);
                }

                // improve the offsprings
                if (dis(rng) < local_search_prob) {
                    auto decoded_offspring1 =
                        encoder_decoder.template DecodeSolution<Solution>(offspring1.chromosome.begin(), offspring1.chromosome.end(), problem);
                    LocalSearchLogger new_logger;
                    new_logger.AddLog(
                        decoded_offspring1.GetQuality(), 1, 1, "Restart solution. Offspring1. Iteration: " + std::to_string(generations));
                    decoded_offspring1 = local_search.FindSolution(new_logger, decoded_offspring1, args...);
#pragma omp critical
                    local_logger += new_logger;
                    offspring1.chromosome.clear();
                    encoder_decoder.EncodeSolution(std::back_inserter(offspring1.chromosome), decoded_offspring1);
                    offspring1.quality = decoded_offspring1.GetQuality();
                } else {
                    if (population.count(offspring1) != 0) {
                        offspring1.quality = population.find(offspring1)->quality;
                    } else {
                        offspring1.quality = encoder_decoder.template EvaluateSolutionQuality<Solution>(
                            offspring1.chromosome.begin(), offspring1.chromosome.end(), problem);
                    }
                }
                if (dis(rng) < local_search_prob) {
                    auto decoded_offspring2 =
                        encoder_decoder.template DecodeSolution<Solution>(offspring2.chromosome.begin(), offspring2.chromosome.end(), problem);
                    LocalSearchLogger new_logger;
                    new_logger.AddLog(
                        decoded_offspring2.GetQuality(), 1, 1, "Restart solution. Offspring2. Iteration: " + std::to_string(generations));
                    decoded_offspring2 = local_search.FindSolution(new_logger, decoded_offspring2, args...);
#pragma omp critical
                    local_logger += new_logger;
                    offspring2.chromosome.clear();
                    encoder_decoder.EncodeSolution(std::back_inserter(offspring2.chromosome), decoded_offspring2);
                    offspring2.quality = decoded_offspring2.GetQuality();
                } else {
                    if (population.count(offspring2) != 0) {
                        offspring2.quality = population.find(offspring2)->quality;
                    } else {
                        offspring2.quality = encoder_decoder.template EvaluateSolutionQuality<Solution>(
                            offspring2.chromosome.begin(), offspring2.chromosome.end(), problem);
                    }
                }

                // select the individuals that will pass to the next generation
                const auto& [descendant1, descendant2] = replacement_op.Choose(parent1, parent2, offspring1, offspring2, rng);

#pragma omp critical
                {
                    // insert the descendants in the new generation
                    new_generation.insert(descendant1);
                    new_generation.insert(descendant2);

                    // check if any of the offsprings is the global best
                    if (descendant1.get().quality > best_solution.quality) {
                        best_solution = descendant1;
                        no_improving_generations = 0;
                    }
                    if (descendant2.get().quality > best_solution.quality) {
                        best_solution = descendant2;
                        no_improving_generations = 0;
                    }
                }
            }
            // set the new generation as the current generation
            population = std::move(new_generation);
            // update average quality
            average_quality =
                std::accumulate(population.begin(), population.end(), 0.0, [](const auto& t1, const auto& t2) { return t1 + t2.quality; }) /
                population.size();

            evolutionary_logger.AddLog(average_quality, best_solution.quality);
        }

        // return the best solution found
        auto best = encoder_decoder.template DecodeSolution<Solution>(best_solution.chromosome.begin(), best_solution.chromosome.end(), problem);
        evolutionary_logger.AddLog(average_quality, best_solution.quality, "Best solution");
        return best;
    }
};

#endif /* MEMETICALGORITHM_HPP_ */
