/**
 * @file jsp_schedule_generation_schemes.hpp
 * @author Pablo
 * @brief JSP Schedule Generation Schemes.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef JSPSCHEDULEGENERATIONSCHEMES_HPP_
#define JSPSCHEDULEGENERATIONSCHEMES_HPP_

#include <algorithm>
#include <iterator>
#include <random>
#include <vector>

#include <external/robin_hood.h>

/**
 * @brief G&T scheduler for JSP.
 * 
 */
class GT
{
  public:
    /**
     * @brief Inserts in a container the earliest starting times of the tasks according to
     * the specified priorities.
     *      
     * @tparam InputIt1 type of the iterator to be used to read the task priorities.
     * @tparam InputIt2 type of the iterator to be used to read the machine assignations.
     * @tparam OutputIt type of the iterator to be used to insert the earliest starting times of the tasks.
     * @tparam Problem type of the problem to be considered.
     * @param first1 iterator pointing to the first task priority.
     * @param last1 iterator pointing to the task priority past the last task priority.
     * @param first2 iterator pointing to the first machine assignation.
     * @param last2 iterator pointing to the machine assignation past the last machine assignation in the encoded solution.
     * @param dest iterator to be used to insert the earliest starting times of the tasks.
     * @param problem problem to be considered.
     * @return the earliest starting times of the tasks.
     */
    template <typename InputIt1, typename InputIt2, typename OutputIt, typename Problem>
    static OutputIt EvaluateSolution(InputIt1 first1, InputIt1 last1, InputIt2 first2, InputIt2 last2, OutputIt dest, const Problem& problem)
    {
        using TaskType = typename Problem::TaskType;
        using MachineType = typename Problem::MachineType;
        using TimeType = typename Problem::TimeType;

        robin_hood::unordered_map<std::reference_wrapper<const TaskType>, std::size_t, std::hash<TaskType>, std::equal_to<TaskType>> priority(
            first1, last1); // priorities of the elements
        robin_hood::unordered_map<std::reference_wrapper<const TaskType>, TimeType, std::hash<TaskType>, std::equal_to<TaskType>>
            est; // earliest starting time of the tasks
        robin_hood::unordered_map<std::reference_wrapper<const MachineType>, TimeType, std::hash<MachineType>, std::equal_to<MachineType>>
            machine_times; // earliest starting time of a new task in the machines
        robin_hood::unordered_map<std::reference_wrapper<const TaskType>,
                                  std::reference_wrapper<const MachineType>,
                                  std::hash<TaskType>,
                                  std::equal_to<TaskType>>
            assignations(first2, last2); // machine assigned to each task

        // final tasks
        std::unordered_set<std::reference_wrapper<const TaskType>, std::hash<TaskType>, std::equal_to<TaskType>> final_tasks;
        problem.GetFinalTasks(std::inserter(final_tasks, final_tasks.begin()));

        // current available tasks
        std::unordered_set<std::reference_wrapper<const TaskType>, std::hash<TaskType>, std::equal_to<TaskType>> available_tasks;
        problem.GetInitialTasks(std::inserter(available_tasks, available_tasks.begin()));

        while (!available_tasks.empty()) {
            // select task with the earliest possible completion time
            const TaskType& candidate_task = *std::min_element(
                available_tasks.begin(), available_tasks.end(), [&est, &machine_times, &assignations](const TaskType& t1, const TaskType& t2) {
                    return std::max(est[t1], machine_times[assignations.at(t1)]) + t1.GetDuration(assignations.at(t1)) <
                           std::max(est[t2], machine_times[assignations.at(t2)]) + t2.GetDuration(assignations.at(t2));
                });

            auto candidate_task_ect = std::max(est[candidate_task], machine_times[assignations.at(candidate_task)]) +
                                      candidate_task.GetDuration(assignations.at(candidate_task));
            // select all tasks in the same machine that may start before the selected task is completed
            std::vector<std::reference_wrapper<const TaskType>> conflict_set;
            std::copy_if(available_tasks.begin(),
                         available_tasks.end(),
                         std::back_inserter(conflict_set),
                         [&est, &machine_times, &assignations, &candidate_task, &candidate_task_ect](const TaskType& t) {
                             return assignations.at(t).get() == assignations.at(candidate_task) &&
                                    std::max(est[t], machine_times[assignations.at(t)]) < candidate_task_ect;
                         });

            // select the task with the highest priority
            const TaskType& current_task =
                *std::min_element(conflict_set.begin(), conflict_set.end(), [&priority](const TaskType& t1, const TaskType& t2) {
                    return priority[t1] < priority[t2];
                });
            available_tasks.erase(current_task);
            auto current_task_est = std::max(est[current_task], machine_times[assignations.at(current_task)]);
            est[current_task] = current_task_est;
            machine_times[assignations.at(current_task)] = current_task_est + current_task.GetDuration(assignations.at(current_task));
            // add the successor to the available tasks set
            if (final_tasks.count(current_task) == 0) {
                const TaskType& next_task = problem.GetTask(current_task.GetJob().GetJobID(), current_task.GetPosition() + 1);
                available_tasks.insert(next_task);
                est[next_task] = current_task_est + current_task.GetDuration(assignations.at(current_task));
            }
            // store the result
            *dest++ = std::make_pair(std::cref(current_task), current_task_est);
        }
        return dest;
    }
};

#endif /* JSPSCHEDULEGENERATIONSCHEMES_HPP_ */