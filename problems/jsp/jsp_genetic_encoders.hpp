/**
 * @file jsp_genetic_encoders.hpp
 * @author Pablo
 * @brief JSP Genetic Encoders.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef JSPGENETICENCODERS_HPP_
#define JSPGENETICENCODERS_HPP_

#include <iterator>
#include <type_traits>
#include <unordered_map>
#include <vector>

#include <problems/jsp/jsp_regular_measures_minimization_solution.hpp>
#include <problems/jsp/jsp_schedule_generation_schemes.hpp>
#include <utils/template_utils.hpp>

/**
 * @brief DualChromosome encoder for JSP. The first chromosome represents the task order
 * and the second chromosome represents the machine assignations.
 * 
 * @tparam Decoder type of the decoder to be used.
 */
template <typename Decoder> class DualChromosomeEncoder
{
  public:
    using GeneType1 = unsigned int;
    using GeneType2 = unsigned int;

  private:
    /**
     * @brief Inserts in a container the tasks and its priorities according to the genes in
     * the encoded solution.
     * 
     * @tparam InputIt type of the iterator to be used to read the encoded solution.
     * @tparam OutputIt type of the iterator to be used to insert the tasks with its priorities.
     * @tparam Problem type of the problem.
     * @param first iterator pointing to the first gene of the encoded solution.
     * @param last iterator pointing to the gene past the last gene of the encoded solution.
     * @param dest iterator to be used to insert the tasks with its priorities.
     * @param problem problem to evaluate.
     * @return an iterator to the pair past the last pair inserted.   
     */
    template <typename InputIt, typename OutputIt, typename Problem>
    static OutputIt CalculatePriorities(InputIt first, InputIt last, OutputIt dest, const Problem& problem)
    {
        std::unordered_map<unsigned int, std::size_t> job_position;
        std::size_t current_priority = 0;
        while (first != last) {
            *dest++ = std::make_pair(std::cref(problem.GetTask(*first, job_position[*first]++)), current_priority++);
            ++first;
        }
        return dest;
    }

  public:
    /**
     * @brief Inserts in a container the genes of the encoded solution.
     * 
     * @tparam Iter1 type of the iterator to be used to insert the genes representing the task order.
     * @tparam Iter2 type of the iterator to be used to insert the genes representing the machine assignations.
     * @tparam Solution type of the solution to be encoded.
     * @param dest1 iterator to be used to insert the genes representing the task order.
     * @param dest2 iterator to be used to insert the genes representing the machine assignations.
     * @param solution solution to be encoded.
     * @return an iterator to the gene past the last gene inserted.    
     */
    template <typename Iter1, typename Iter2, typename Solution>
    static std::tuple<Iter1, Iter2> EncodeSolution(Iter1 dest1, Iter2 dest2, const Solution& solution)
    {
        using TaskType = typename Solution::TaskType;
        // get the tasks in topological order
        std::vector<std::reference_wrapper<const TaskType>> tasks;
        solution.GetTasksTopologicalOrder(std::back_inserter(tasks));
        // substitute each task with the identifier of its job
        auto iter1 = std::transform(tasks.begin(), tasks.end(), dest1, [](const TaskType& task) { return task.GetJob().GetJobID(); });
        // sort the tasks by id
        std::sort(tasks.begin(), tasks.end(), [](const TaskType& t1, const TaskType& t2) { return t1.GetTaskID() < t2.GetTaskID(); });
        // substitute each task with the identifier of its assigned machine
        auto iter2 = std::transform(
            tasks.begin(), tasks.end(), dest2, [&solution](const TaskType& task) { return solution.GetAssignedMachine(task).GetMachineID(); });
        return std::make_tuple(iter1, iter2);
    }

    /**
     * @brief Decodes an encoded solution and updates the chromosome accordingly.
     * 
     * @tparam Solution type of the solution to be decoded.
     * @tparam Iter1 type of the iterator to be used to read the genes representing the task order.
     * @tparam Iter2 type of the iterator to be used to read the genes representing the machine assignations.
     * @tparam Problem type of the problem to be decoded.
     * @param first1 iterator pointing to the first gene representing the task order.
     * @param last1 iterator pointing to the gene past the last gene representing the task order.
     * @param first2 iterator pointing to the first gene representing the machine assignations.
     * @param last2 iterator pointing to the gene past the last gene representing the machine assignations.
     * @param problem problem to which the solution to be decoded belongs.
     * @return the decoded solution.
     */
    template <typename Solution, typename Iter1, typename Iter2, typename Problem>
    static Solution DecodeSolution(Iter1 first1, Iter1 last1, Iter2 first2, Iter2, const Problem& problem)
    {
        using TaskType = typename Problem::TaskType;
        using MachineType = typename Problem::MachineType;
        using TimeType = typename Problem::TimeType;
        // build the machine assignations
        std::vector<std::reference_wrapper<const TaskType>> tasks;
        problem.GetTasks(std::back_inserter(tasks));
        std::sort(tasks.begin(), tasks.end(), [](const TaskType& t1, const TaskType& t2) { return t1.GetTaskID() < t2.GetTaskID(); });
        std::unordered_map<std::reference_wrapper<const TaskType>,
                           std::reference_wrapper<const MachineType>,
                           std::hash<TaskType>,
                           std::equal_to<TaskType>>
            assignations;
        for (const TaskType& task: tasks) {
            assignations.insert(std::make_pair(std::cref(task), std::cref(problem.GetMachine(*first2++))));
        }
        // calculate the priorities of the tasks
        std::unordered_map<std::reference_wrapper<const TaskType>, std::size_t, std::hash<TaskType>, std::equal_to<TaskType>> priorities;
        CalculatePriorities(first1, last1, std::inserter(priorities, priorities.begin()), problem);
        // calculate the earliest starting time of the tasks
        std::vector<std::pair<std::reference_wrapper<const TaskType>, TimeType>> est;
        Decoder::EvaluateSolution(std::make_move_iterator(priorities.begin()),
                                  std::make_move_iterator(priorities.end()),
                                  assignations.begin(),
                                  assignations.end(),
                                  std::inserter(est, est.begin()),
                                  problem);
        // update encoding with the new values
        std::transform(est.begin(), est.end(), first1, [](const auto& t) { return t.first.get().GetJob().GetJobID(); });
        // build the solution
        return Solution{std::make_move_iterator(est.begin()),
                        std::make_move_iterator(est.end()),
                        std::make_move_iterator(assignations.begin()),
                        std::make_move_iterator(assignations.end()),
                        problem};
    }

    /**
     * @brief Returns the makespan of an encoded solution and updates the chromosome accordingly.
     * 
     * @tparam Solution type of the solution to be evaluated.
     * @tparam Iter1 type of the iterator to be used to read the genes representing the task order.
     * @tparam Iter2 type of the iterator to be used to read the genes representing the machine assignations.
     * @tparam Problem type of the problem to be evaluated.
     * @param first1 iterator pointing to the first gene representing the task order.
     * @param last1 iterator pointing to the gene past the last gene representing the task order.
     * @param first2 iterator pointing to the first gene representing the machine assignations.
     * @param last2 iterator pointing to the gene past the last gene representing the machine assignations.
     * @param problem problem to which the solution to be decoded belongs.
     * @return the makespan of the solution.
     */
    template <typename Solution, typename Iter1, typename Iter2, typename Problem>
    static typename Problem::TimeType EvaluateSolutionMakespan(Iter1 first1, Iter1 last1, Iter2 first2, Iter2, const Problem& problem)
    {
        using TaskType = typename Problem::TaskType;
        using MachineType = typename Problem::MachineType;
        using TimeType = typename Problem::TimeType;
        // build the machine assignations
        std::vector<std::reference_wrapper<const TaskType>> tasks;
        problem.GetTasks(std::back_inserter(tasks));
        std::sort(tasks.begin(), tasks.end(), [](const TaskType& t1, const TaskType& t2) { return t1.GetTaskID() < t2.GetTaskID(); });
        std::unordered_map<std::reference_wrapper<const TaskType>,
                           std::reference_wrapper<const MachineType>,
                           std::hash<TaskType>,
                           std::equal_to<TaskType>>
            assignations;
        for (const TaskType& task: tasks) {
            assignations.insert(std::make_pair(std::cref(task), std::cref(problem.GetMachine(*first2++))));
        }
        // calculate the priorities of the tasks
        std::unordered_map<std::reference_wrapper<const TaskType>, std::size_t, std::hash<TaskType>, std::equal_to<TaskType>> priorities;
        CalculatePriorities(first1, last1, std::inserter(priorities, priorities.begin()), problem);
        // calculate the earliest starting time of the tasks
        std::vector<std::pair<std::reference_wrapper<const TaskType>, TimeType>> ordered_est;
        Decoder::EvaluateSolution(std::make_move_iterator(priorities.begin()),
                                  std::make_move_iterator(priorities.end()),
                                  assignations.begin(),
                                  assignations.end(),
                                  std::inserter(ordered_est, ordered_est.begin()),
                                  problem);
        // update encoding with the new values
        std::transform(ordered_est.begin(), ordered_est.end(), first1, [](const auto& t) { return t.first.get().GetJob().GetJobID(); });
        // store the earliest starting times in a map
        std::unordered_map<std::reference_wrapper<const TaskType>, TimeType, std::hash<TaskType>, std::equal_to<TaskType>> est(ordered_est.begin(),
                                                                                                                               ordered_est.end());
        // get the final tasks
        std::vector<std::reference_wrapper<const TaskType>> final_tasks;
        problem.GetFinalTasks(std::back_inserter(final_tasks));
        // calculate the makespan
        TimeType makespan{};
        for (const TaskType& task: final_tasks) {
            makespan = std::max(makespan, task.GetDuration(assignations.at(task)) + est.at(task));
        }
        return makespan;
    }

    /**
     * @brief Returns the total weighted tardiness of an encoded solution and updates the chromosome accordingly.
     * 
     * @tparam Solution type of the solution to be evaluated.
     * @tparam Iter1 type of the iterator to be used to read the genes representing the task order.
     * @tparam Iter2 type of the iterator to be used to read the genes representing the machine assignations.
     * @tparam Problem type of the problem to be evaluated.
     * @param first1 iterator pointing to the first gene representing the task order.
     * @param last1 iterator pointing to the gene past the last gene representing the task order.
     * @param first2 iterator pointing to the first gene representing the machine assignations.
     * @param last2 iterator pointing to the gene past the last gene representing the machine assignations.
     * @param problem problem to which the solution to be decoded belongs.
     * @return the total weighted tardiness of the solution.
     */
    template <typename Solution, typename Iter1, typename Iter2, typename Problem>
    static typename Problem::TimeType EvaluateSolutionTotalWeightedTardiness(Iter1 first1, Iter1 last1, Iter2 first2, Iter2, const Problem& problem)
    {
        using TaskType = typename Problem::TaskType;
        using MachineType = typename Problem::MachineType;
        using TimeType = typename Problem::TimeType;
        // build the machine assignations
        std::vector<std::reference_wrapper<const TaskType>> tasks;
        problem.GetTasks(std::back_inserter(tasks));
        std::sort(tasks.begin(), tasks.end(), [](const TaskType& t1, const TaskType& t2) { return t1.GetTaskID() < t2.GetTaskID(); });
        std::unordered_map<std::reference_wrapper<const TaskType>,
                           std::reference_wrapper<const MachineType>,
                           std::hash<TaskType>,
                           std::equal_to<TaskType>>
            assignations;
        for (const TaskType& task: tasks) {
            assignations.insert(std::make_pair(std::cref(task), std::cref(problem.GetMachine(*first2++))));
        }
        // calculate the priorities of the tasks
        std::unordered_map<std::reference_wrapper<const TaskType>, std::size_t, std::hash<TaskType>, std::equal_to<TaskType>> priorities;
        CalculatePriorities(first1, last1, std::inserter(priorities, priorities.begin()), problem);
        // calculate the earliest starting time of the tasks
        std::vector<std::pair<std::reference_wrapper<const TaskType>, TimeType>> ordered_est;
        Decoder::EvaluateSolution(std::make_move_iterator(priorities.begin()),
                                  std::make_move_iterator(priorities.end()),
                                  assignations.begin(),
                                  assignations.end(),
                                  std::inserter(ordered_est, ordered_est.begin()),
                                  problem);
        // update encoding with the new values
        std::transform(ordered_est.begin(), ordered_est.end(), first1, [](const auto& t) { return t.first.get().GetJob().GetJobID(); });
        // store the earliest starting times in a map
        std::unordered_map<std::reference_wrapper<const TaskType>, TimeType, std::hash<TaskType>, std::equal_to<TaskType>> est(ordered_est.begin(),
                                                                                                                               ordered_est.end());
        // get the final tasks
        std::vector<std::reference_wrapper<const TaskType>> final_tasks;
        problem.GetFinalTasks(std::back_inserter(final_tasks));
        // calculate the total weighted tardiness
        TimeType twt{};
        for (const TaskType& task: final_tasks) {
            auto job = task.GetJob();
            auto tardiness = est.at(task) + task.GetDuration(assignations.at(task)) - job.GetDueDate();
            twt += std::max(TimeType{}, tardiness) * job.GetWeight();
        }
        return twt;
    }

    /**
     * @brief Returns the quality of an encoded solution.
     * 
     * @tparam Solution type of the solution to be evaluated.
     * @tparam Iter1 type of the iterator to be used to read the genes representing the task order.
     * @tparam Iter2 type of the iterator to be used to read the genes representing the machine assignations.
     * @tparam Problem type of the problem to be evaluated.
     * @param first1 iterator pointing to the first gene representing the task order.
     * @param last1 iterator pointing to the gene past the last gene representing the task order.
     * @param first2 iterator pointing to the first gene representing the machine assignations.
     * @param last2 iterator pointing to the gene past the last gene representing the machine assignations.
     * @param problem problem to which the solution to be decoded belongs.
     * @return the quality of the solution.
     */
    template <typename Solution, typename Iter1, typename Iter2, typename Problem>
    static double EvaluateSolutionQuality(Iter1 first1, Iter1 last1, Iter2 first2, Iter2 last2, const Problem& problem)
    {
        // return the quality of the solution depending on the solution type
        if constexpr (Solution::GetMeasure() == 0) {
            return 1.0 / EvaluateSolutionMakespan<Solution>(first1, last1, first2, last2, problem);
        } else if constexpr (Solution::GetMeasure() == 1) {
            return 1.0 / EvaluateSolutionTotalWeightedTardiness<Solution>(first1, last1, first2, last2, problem);
        }
    }
};

/**
 * @brief DualGene encoder for JSP. The first gene represents the task order
 * and the second gene represents the machine assignations.
 * 
 * @tparam Decoder type of the decoder to be used.
 */
template <typename Decoder> class DualGeneEncoder
{
  public:
    using GeneType = std::pair<unsigned int, unsigned int>;

  private:
    /**
     * @brief Transforms the chromosome into a task order and a machine assignation.
     * 
     * @tparam InputIt type of the iterator to be used to read the encoded solution.
     * @tparam OutputIt1 type of the iterator to be used to insert the tasks with its priorities.
     * @tparam OutputIt2 type of the iterator to be used to insert the machine assignations.
     * @tparam Problem type of the problem.
     * @param first iterator pointing to the first gene of the encoded solution.
     * @param last iterator pointing to the gene past the last gene of the encoded solution.
     * @param dest1 iterator to be used to insert the tasks with its priorities.
     * @param dest2 iterator to be used to insert the machine assignations.
     * @param problem problem to evaluate.
     * @return a pair with the iterators to the element past the last element inserted. 
     */
    template <typename InputIt, typename OutputIt1, typename OutputIt2, typename Problem>
    static std::pair<OutputIt1, OutputIt2> TransformChromosome(InputIt first, InputIt last, OutputIt1 dest1, OutputIt2 dest2, const Problem& problem)
    {
        std::unordered_map<unsigned int, std::size_t> job_position;
        std::size_t current_priority = 0;
        while (first != last) {
            auto& task = problem.GetTask(first->first, job_position[first->first]++);
            *dest1++ = std::make_pair(std::cref(task), current_priority++);
            *dest2++ = std::make_pair(std::cref(task), std::cref(problem.GetMachine(first->second)));
            ++first;
        }
        return std::make_pair(dest1, dest2);
    }

  public:
    /**
     * @brief Inserts in a container the genes of the encoded solution.
     * 
     * @tparam Iter type of the iterator to be used to insert the genes representing the solution.
     * @tparam Solution type of the solution to be encoded.
     * @param dest iterator to be used to insert the genes representing the solution.
     * @param solution solution to be encoded.
     * @return an iterator to the gene past the last gene inserted.    
     */
    template <typename Iter, typename Solution> static Iter EncodeSolution(Iter dest, const Solution& solution)
    {
        using TaskType = typename Solution::TaskType;
        // get the tasks in topological order
        std::vector<std::reference_wrapper<const TaskType>> tasks;
        solution.GetTasksTopologicalOrder(std::back_inserter(tasks));
        // substitute each task with the identifier of its job and the identifier of its assigned machine
        return std::transform(tasks.begin(), tasks.end(), dest, [&solution](const TaskType& task) {
            return std::make_pair(task.GetJob().GetJobID(), solution.GetAssignedMachine(task).GetMachineID());
        });
    }

    /**
     * @brief Decodes an encoded solution and updates the chromosome accordingly.
     * 
     * @tparam Solution type of the solution to be decoded.
     * @tparam Iter type of the iterator to be used to read the genes representing the solution.    
     * @tparam Problem type of the problem to be decoded.
     * @param first iterator pointing to the first gene representing the solution.
     * @param last iterator pointing to the gene past the last gene representing the solution.
     * @param problem problem to which the solution to be decoded belongs.
     * @return the decoded solution.
     */
    template <typename Solution, typename Iter, typename Problem> static Solution DecodeSolution(Iter first, Iter last, const Problem& problem)
    {
        using TaskType = typename Problem::TaskType;
        using MachineType = typename Problem::MachineType;
        using TimeType = typename Problem::TimeType;
        std::unordered_map<std::reference_wrapper<const TaskType>,
                           std::reference_wrapper<const MachineType>,
                           std::hash<TaskType>,
                           std::equal_to<TaskType>>
            assignations;
        std::unordered_map<std::reference_wrapper<const TaskType>, std::size_t, std::hash<TaskType>, std::equal_to<TaskType>> priorities;
        TransformChromosome(first, last, std::inserter(priorities, priorities.begin()), std::inserter(assignations, assignations.begin()), problem);
        // calculate the earliest starting time of the tasks
        std::vector<std::pair<std::reference_wrapper<const TaskType>, TimeType>> est;
        Decoder::EvaluateSolution(std::make_move_iterator(priorities.begin()),
                                  std::make_move_iterator(priorities.end()),
                                  assignations.begin(),
                                  assignations.end(),
                                  std::inserter(est, est.begin()),
                                  problem);
        // update encoding with the new values
        std::transform(est.begin(), est.end(), first, [&assignations](const auto& t) {
            return std::make_pair(t.first.get().GetJob().GetJobID(), assignations.at(t.first).get().GetMachineID());
        });
        // build the solution
        return Solution{std::make_move_iterator(est.begin()),
                        std::make_move_iterator(est.end()),
                        std::make_move_iterator(assignations.begin()),
                        std::make_move_iterator(assignations.end()),
                        problem};
    }

    /**
     * @brief Returns the makespan of an encoded solution and updates the chromosome accordingly.
     * 
     * @tparam Solution type of the solution to be evaluated.
     * @tparam Iter type of the iterator to be used to read the genes representing the solution.     
     * @tparam Problem type of the problem to be evaluated.
     * @param first iterator pointing to the first gene representing the solution.
     * @param last iterator pointing to the gene past the last gene representing the solution.     
     * @param problem problem to which the solution to be decoded belongs.
     * @return the makespan of the solution.
     */
    template <typename Solution, typename Iter, typename Problem>
    static typename Problem::TimeType EvaluateSolutionMakespan(Iter first, Iter last, const Problem& problem)
    {
        using TaskType = typename Problem::TaskType;
        using MachineType = typename Problem::MachineType;
        using TimeType = typename Problem::TimeType;
        std::unordered_map<std::reference_wrapper<const TaskType>,
                           std::reference_wrapper<const MachineType>,
                           std::hash<TaskType>,
                           std::equal_to<TaskType>>
            assignations;
        std::unordered_map<std::reference_wrapper<const TaskType>, std::size_t, std::hash<TaskType>, std::equal_to<TaskType>> priorities;
        TransformChromosome(first, last, std::inserter(priorities, priorities.begin()), std::inserter(assignations, assignations.begin()), problem);
        // calculate the earliest starting time of the tasks
        std::vector<std::pair<std::reference_wrapper<const TaskType>, TimeType>> ordered_est;
        Decoder::EvaluateSolution(std::make_move_iterator(priorities.begin()),
                                  std::make_move_iterator(priorities.end()),
                                  assignations.begin(),
                                  assignations.end(),
                                  std::inserter(ordered_est, ordered_est.begin()),
                                  problem);
        // update encoding with the new values
        std::transform(ordered_est.begin(), ordered_est.end(), first, [&assignations](const auto& t) {
            return std::make_pair(t.first.get().GetJob().GetJobID(), assignations.at(t.first).get().GetMachineID());
        });
        // store the earliest starting times in a map
        std::unordered_map<std::reference_wrapper<const TaskType>, TimeType, std::hash<TaskType>, std::equal_to<TaskType>> est(ordered_est.begin(),
                                                                                                                               ordered_est.end());
        // get the final tasks
        std::vector<std::reference_wrapper<const TaskType>> final_tasks;
        problem.GetFinalTasks(std::back_inserter(final_tasks));
        // calculate the makespan
        TimeType makespan{};
        for (const TaskType& task: final_tasks) {
            makespan = std::max(makespan, task.GetDuration(assignations.at(task)) + est.at(task));
        }
        return makespan;
    }

    /**
     * @brief Returns the total weighted tardiness of an encoded solution and updates the chromosome accordingly.
     * 
     * @tparam Solution type of the solution to be evaluated.
     * @tparam Iter type of the iterator to be used to read the genes representing the solution.
     * @tparam Problem type of the problem to be evaluated.
     * @param first iterator pointing to the first gene representing the solution.
     * @param last iterator pointing to the gene past the last gene representing the solution.
     * @param problem problem to which the solution to be decoded belongs.
     * @return the total weighted tardiness of the solution.
     */
    template <typename Solution, typename Iter, typename Problem>
    static typename Problem::TimeType EvaluateSolutionTotalWeightedTardiness(Iter first, Iter last, const Problem& problem)
    {
        using TaskType = typename Problem::TaskType;
        using MachineType = typename Problem::MachineType;
        using TimeType = typename Problem::TimeType;
        std::unordered_map<std::reference_wrapper<const TaskType>,
                           std::reference_wrapper<const MachineType>,
                           std::hash<TaskType>,
                           std::equal_to<TaskType>>
            assignations;
        std::unordered_map<std::reference_wrapper<const TaskType>, std::size_t, std::hash<TaskType>, std::equal_to<TaskType>> priorities;
        TransformChromosome(first, last, std::inserter(priorities, priorities.begin()), std::inserter(assignations, assignations.begin()), problem);
        // calculate the earliest starting time of the tasks
        std::vector<std::pair<std::reference_wrapper<const TaskType>, TimeType>> ordered_est;
        Decoder::EvaluateSolution(std::make_move_iterator(priorities.begin()),
                                  std::make_move_iterator(priorities.end()),
                                  assignations.begin(),
                                  assignations.end(),
                                  std::inserter(ordered_est, ordered_est.begin()),
                                  problem);
        // update encoding with the new values
        std::transform(ordered_est.begin(), ordered_est.end(), first, [&assignations](const auto& t) {
            return std::make_pair(t.first.get().GetJob().GetJobID(), assignations.at(t.first).get().GetMachineID());
        });
        // store the earliest starting times in a map
        std::unordered_map<std::reference_wrapper<const TaskType>, TimeType, std::hash<TaskType>, std::equal_to<TaskType>> est(ordered_est.begin(),
                                                                                                                               ordered_est.end());
        // get the final tasks
        std::vector<std::reference_wrapper<const TaskType>> final_tasks;
        problem.GetFinalTasks(std::back_inserter(final_tasks));
        // calculate the total weighted tardiness
        TimeType twt{};
        for (const TaskType& task: final_tasks) {
            auto job = task.GetJob();
            auto tardiness = est.at(task) + task.GetDuration(assignations.at(task)) - job.GetDueDate();
            twt += std::max(TimeType{}, tardiness) * job.GetWeight();
        }
        return twt;
    }

    /**
     * @brief Returns the quality of an encoded solution.
     * 
     * @tparam Solution type of the solution to be evaluated.
     * @tparam Iter type of the iterator to be used to read the genes representing the solution.
     * @tparam Problem type of the problem to be evaluated.
     * @param first iterator pointing to the first gene representing the solution.
     * @param last iterator pointing to the gene past the last gene representing the solution.
     * @param problem problem to which the solution to be decoded belongs.
     * @return the quality of the solution.
     */
    template <typename Solution, typename Iter, typename Problem> static double EvaluateSolutionQuality(Iter first, Iter last, const Problem& problem)
    {
        // return the quality of the solution depending on the solution type
        if constexpr (Solution::GetMeasure() == 0) {
            return 1.0 / EvaluateSolutionMakespan<Solution>(first, last, problem);
        } else if constexpr (Solution::GetMeasure() == 1) {
            return 1.0 / EvaluateSolutionTotalWeightedTardiness<Solution>(first, last, problem);
        }
    }
};

#endif /* JSPGENETICENCODERS_HPP_ */