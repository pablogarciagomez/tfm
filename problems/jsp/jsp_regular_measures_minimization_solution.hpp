/**
 * @file jsp_regular_measures_minimization_solution.hpp
 * @author Pablo
 * @brief JSP Regular Measures Minimization.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef JSPREGULARMEASURESMINIMIZATIONSOLUTION_HPP_
#define JSPREGULARMEASURESMINIMIZATIONSOLUTION_HPP_

#include <optional>
#include <queue>
#include <sstream>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include <external/robin_hood.h>
#include <problems/jsp/jsp_critical_block.hpp>
#include <utils/template_utils.hpp>
#include <utils/triangular_fuzzy_number.hpp>

/**
 * @brief Solution to a JSP minimizing a regular measure.
 * 
 * @tparam Problem type of the problem to be solved.
 * @tparam Measure regular measure to minimize, 0 makespan, 1 total weighted tardiness
 * @tparam Tails enables tails.
 */
template <typename Problem, typename Measure = std::integral_constant<int, 0>, typename Tails = std::false_type>
class JSPRegularMeasuresMinimizationSolution
{
  public:
    using ProblemType = Problem; // type of the problem to be solved
    using TaskType = typename ProblemType::TaskType; // type of the tasks to be scheduled
    using JobType = typename ProblemType::JobType; // type of the jobs
    using MachineType = typename ProblemType::MachineType; // type of the machines
    using TimeType = typename TaskType::TimeType; // type of the time unit
  private:
    std::reference_wrapper<const ProblemType> problem; // problem to be solved
    std::vector<std::reference_wrapper<const TaskType>> tasks;
    std::vector<std::reference_wrapper<const JobType>> jobs;

    robin_hood::unordered_map<std::reference_wrapper<const JobType>,
                              std::vector<std::reference_wrapper<const TaskType>>,
                              std::hash<JobType>,
                              std::equal_to<JobType>>
        job_order; // order of the tasks in a job
    robin_hood::unordered_map<std::reference_wrapper<const MachineType>,
                              std::vector<std::reference_wrapper<const TaskType>>,
                              std::hash<MachineType>,
                              std::equal_to<MachineType>>
        machine_order; // order of the tasks in a machine
    mutable std::vector<std::reference_wrapper<const TaskType>> processing_order; // procesing order of the tasks

    robin_hood::unordered_map<std::reference_wrapper<const TaskType>, std::size_t, std::hash<TaskType>, std::equal_to<TaskType>>
        job_position; // position of the tasks in a job
    robin_hood::unordered_map<std::reference_wrapper<const TaskType>, std::size_t, std::hash<TaskType>, std::equal_to<TaskType>>
        machine_position; // position of the tasks in a machine
    mutable robin_hood::unordered_map<std::reference_wrapper<const TaskType>, std::size_t, std::hash<TaskType>, std::equal_to<TaskType>>
        global_position; // position of the tasks in the topological order

    mutable std::unordered_map<std::reference_wrapper<const TaskType>, TimeType, std::hash<TaskType>, std::equal_to<TaskType>>
        heads; // heads of the tasks

    mutable robin_hood::unordered_map<
        std::reference_wrapper<const TaskType>,
        robin_hood::unordered_map<std::reference_wrapper<const JobType>, std::optional<TimeType>, std::hash<JobType>, std::equal_to<JobType>>,
        std::hash<TaskType>,
        std::equal_to<TaskType>>
        tails; // tails of the tasks
    std::reference_wrapper<const JobType> dummy; // dummy job for tails calculation

    mutable robin_hood::unordered_set<std::reference_wrapper<const TaskType>, std::hash<TaskType>, std::equal_to<TaskType>>
        changes; // tasks that have changed since the last heads and tails update
    mutable bool topological_order_changed;

    robin_hood::
        unordered_map<std::reference_wrapper<const TaskType>, std::reference_wrapper<const MachineType>, std::hash<TaskType>, std::equal_to<TaskType>>
            assigned_machine; // assigned machines

    mutable TimeType makespan; // current makespan
    mutable TimeType total_weighted_tardiness; // current total weighted tardiness
    mutable double quality; // solution quality

  public:
    /**
     * @brief Constructs a new JSPRegularMeasuresMinimizationSolution.
     * 
     * @param problem problem to be solved.
     */
    JSPRegularMeasuresMinimizationSolution(const ProblemType& problem) :
        problem{problem},
        job_order(problem.GetNumberOfJobs()),
        machine_order(problem.GetNumberOfMachines()),
        job_position(problem.GetNumberOfTasks()),
        machine_position(problem.GetNumberOfTasks()),
        heads(problem.GetNumberOfTasks()),
        tails(problem.GetNumberOfTasks()),
        dummy{problem.GetJob(1)}
    {
        problem.GetTasks(std::back_inserter(tasks));
        std::sort(tasks.begin(), tasks.end(), [](const TaskType& t1, const TaskType& t2) { return t1.GetTaskID() < t2.GetTaskID(); });
        problem.GetJobs(std::back_inserter(jobs));
        std::sort(jobs.begin(), jobs.end(), [](const JobType& j1, const JobType& j2) { return j1.GetJobID() < j2.GetJobID(); });

        // add job precedence restrictions
        for (const JobType& job: jobs) {
            problem.GetJobTasks(std::back_inserter(job_order[job]), job.GetJobID());
            for (std::size_t i = 0; i < job_order[job].size(); i++) {
                job_position[job_order[job].at(i)] = i;
                changes.insert(job_order[job].at(i));
            }
        }

        if constexpr (Tails::value) {
            // prepare tails to be calculated depending on the measure
            if constexpr (Measure::value == 0) {
                for (const TaskType& task: tasks) {
                    tails[task][dummy] = TimeType{};
                }
            } else {
                for (const TaskType& task: tasks) {
                    for (const JobType& job: jobs) {
                        tails[task][job] = task.GetJob() == job ? std::make_optional(TimeType{}) : std::nullopt;
                    }
                }
            }
        }

        topological_order_changed = true;
    }

    /**
     * @brief Construct a new JSPRegularMeasuresMinimizationSolution.
     * 
     * @tparam Iter type of the iterator to be used to read the priorities.
     * @param first iterator pointing to the first task priority.
     * @param last iterator pointing to the task priority past the last task priority.
     * @param problem problem to be considered.
     */
    template <typename Iter1, typename Iter2>
    JSPRegularMeasuresMinimizationSolution(Iter1 first1, Iter1 last1, Iter2 first2, Iter2 last2, const ProblemType& problem) :
        problem{problem},
        job_order(problem.GetNumberOfJobs()),
        machine_order(problem.GetNumberOfMachines()),
        job_position(problem.GetNumberOfTasks()),
        machine_position(problem.GetNumberOfTasks()),
        heads(problem.GetNumberOfTasks()),
        tails(problem.GetNumberOfTasks()),
        dummy{problem.GetJob(1)},
        assigned_machine(first2, last2)
    {
        problem.GetTasks(std::back_inserter(tasks));
        std::sort(tasks.begin(), tasks.end(), [](const TaskType& t1, const TaskType& t2) { return t1.GetTaskID() < t2.GetTaskID(); });
        problem.GetJobs(std::back_inserter(jobs));
        std::sort(jobs.begin(), jobs.end(), [](const JobType& j1, const JobType& j2) { return j1.GetJobID() < j2.GetJobID(); });

        // add job precedence restrictions
        for (const JobType& job: jobs) {
            problem.GetJobTasks(std::back_inserter(job_order[job]), job.GetJobID());
            for (std::size_t i = 0; i < job_order[job].size(); i++) {
                job_position[job_order[job].at(i)] = i;
                changes.insert(job_order[job].at(i));
            }
        }

        if constexpr (Tails::value) {
            // prepare tails to be calculated depending on the measure
            if constexpr (Measure::value == 0) {
                for (const TaskType& task: tasks) {
                    tails[task][dummy] = TimeType{};
                }
            } else {
                for (const TaskType& task: tasks) {
                    for (const JobType& job: jobs) {
                        tails[task][job] = task.GetJob() == job ? std::make_optional(TimeType{}) : std::nullopt;
                    }
                }
            }
        }

        robin_hood::unordered_map<TaskType, typename std::iterator_traits<Iter1>::value_type::second_type> priorities(first1, last1);

        // add machine restrictions
        problem.GetTasks(std::back_inserter(processing_order));
        std::sort(
            processing_order.begin(), processing_order.end(), [&priorities](const auto& a, const auto& b) { return priorities[a] < priorities[b]; });
        for (const TaskType& task: processing_order) {
            machine_position[task] = machine_order[assigned_machine.at(task)].size();
            machine_order[assigned_machine.at(task)].push_back(task);
        }
        topological_order_changed = true;
    }

  private:
    /**
     * @brief Updates the processing order.
     * 
     */
    void UpdateTopologicalOrder() const
    {
        if (!topological_order_changed) {
            return;
        }
        robin_hood::unordered_map<std::reference_wrapper<const TaskType>, unsigned short, std::hash<TaskType>, std::equal_to<TaskType>> in_degree;
        std::queue<std::reference_wrapper<const TaskType>> available;
        std::size_t cnt = 0;

        for (const auto& task: tasks) {
            auto [job_predecessor, machine_predecessor] = GetPredecessors(task);
            auto d = (job_predecessor.has_value() ? 1 : 0) + (machine_predecessor.has_value() ? 1 : 0);
            in_degree[task] = d;
            if (d == 0) {
                available.push(task);
            }
        }
        processing_order.clear();

        while (!available.empty()) {
            const TaskType& current = available.front();
            available.pop();
            processing_order.push_back(current);
            global_position[current] = cnt++;
            auto [job_successor, machine_successor] = GetSuccessors(current);
            if (job_successor.has_value()) {
                if (--in_degree[*job_successor] == 0) {
                    available.push(*job_successor);
                }
            }
            if (machine_successor.has_value()) {
                if (--in_degree[*machine_successor] == 0) {
                    available.push(*machine_successor);
                }
            }
        }

        if (cnt != GetProblem().GetNumberOfTasks()) {
            throw std::invalid_argument("Not a DAG");
        }
        topological_order_changed = false;
    }

    /**
     * @brief Updates the head of all the tasks that have been affected by modifications since the last call.
     * 
     */
    void UpdateHeads() const
    {
        // update the heads
        // find the first task whose head has to be updated
        auto min = std::min_element(changes.begin(), changes.end(), [this](const TaskType& t1, const TaskType& t2) {
            return global_position.at(t1) < global_position.at(t2);
        });
        auto it = processing_order.begin() + global_position.at(*min);
        // calculate the head of the tasks that are scheduled after the first modified task
        while (it != processing_order.end()) {
            auto [job_predecessor, machine_predecessor] = GetPredecessors(*it);
            heads[*it] = TimeType{};
            if (job_predecessor.has_value()) {
                heads[*it] = std::max(heads.at(*it), heads.at(*job_predecessor) + GetDuration(*job_predecessor));
            }
            if (machine_predecessor.has_value()) {
                heads[*it] = std::max(heads.at(*it), heads.at(*machine_predecessor) + GetDuration(*machine_predecessor));
            }
            ++it;
        }
    }

    /**
     * @brief Updates the tails of all the tasks that have been affected by modifications since the last call.
     * 
     */
    void UpdateTails() const
    {
        // find "the last" task whose tails have to be updated
        auto max = std::max_element(changes.begin(), changes.end(), [this](const TaskType& t1, const TaskType& t2) {
            return global_position.at(t1) < global_position.at(t2);
        });
        auto rit = processing_order.rbegin() + (global_position.size() - global_position.at(*max) - 1);
        // calculate the tails of the tasks that are scheduled before "the last" modified task
        while (rit != processing_order.rend()) {
            auto [job_successor, machine_successor] = GetSuccessors(*rit);
            for (auto& [job, tail]: tails[*rit]) {
                if constexpr (Measure::value == 0) {
                    tail = TimeType{};
                } else {
                    tail = rit->get().GetJob() == job ? std::make_optional(TimeType{}) : std::nullopt;
                }
                if (job_successor.has_value() && tails.at(*job_successor).at(job).has_value()) {
                    tail = *tails.at(*job_successor).at(job) + GetDuration(*job_successor);
                }
                if (machine_successor.has_value() && tails.at(*machine_successor).at(job).has_value()) {
                    tail = tails.at(*rit).at(job).has_value()
                               ? std::max(*tails.at(*rit).at(job), *tails.at(*machine_successor).at(job) + GetDuration(*machine_successor))
                               : *tails.at(*machine_successor).at(job) + GetDuration(*machine_successor);
                }
            }
            ++rit;
        }
    }

    /**
     * @brief Updates the makespan.
     * 
     */
    void UpdateMakespan() const
    {
        makespan = TimeType{};
        for (const auto& [job, ordered_tasks]: job_order) {
            makespan = std::max(makespan, GetDuration(ordered_tasks.back()) + heads.at(ordered_tasks.back().get()));
        }
    }

    /**
     * @brief Updates the total weighted tardiness.
     * 
     */
    void UpdateTotalWeightedTardiness() const
    {
        total_weighted_tardiness = TimeType{};
        for (const auto& [job, ordered_tasks]: job_order) {
            auto tardiness = heads[ordered_tasks.back()] + GetDuration(ordered_tasks.back()) - job.get().GetDueDate();
            total_weighted_tardiness += std::max(TimeType{}, tardiness) * job.get().GetWeight();
        }
    }

    /**
     * @brief Updates the lazy values.
     * 
     */
    void Update() const
    {
        if (changes.empty()) {
            return;
        }
        UpdateTopologicalOrder();
        UpdateHeads();
        if constexpr (Tails::value) {
            UpdateTails();
        }
        // update measures
        if constexpr (Measure::value == 0) {
            UpdateMakespan();
            quality = 1.0 / (double)makespan;
        } else {
            UpdateTotalWeightedTardiness();
            if (total_weighted_tardiness == TimeType{}) {
                quality = std::numeric_limits<double>::max();
            } else {
                quality = 1.0 / (double)total_weighted_tardiness;
            }
        }
        changes.clear();
    }

    /**
     * @brief Checks if two numbers are equal.
     * 
     * @param n1 first number.
     * @param n2 second number.
     * @return true if the numbers are equal, false in other case.
     */
    bool EqualTime(const TimeType& n1, const TimeType& n2) const
    {
        if constexpr (is_specialization<TimeType, TriangularFuzzyNumber>::value) {
            return n1.GetSmallest() == n2.GetSmallest() || n1.GetMostProbable() == n2.GetMostProbable() || n1.GetLargest() == n2.GetLargest();
        } else {
            return n1 == n2;
        }
    };

  public:
    /**
     * @brief Returns the makespan.
     * 
     * @return the makespan of the solution. 
     */
    TimeType GetMakespan() const
    {
        static_assert(Measure::value == 0, "Measure must be 0");
        Update();
        return makespan;
    }

    /**
     * @brief Returns the total weighted tardiness.
     * 
     * @return the total weighted tardiness of the solution. 
     */
    TimeType GetTotalWeightedTardiness() const
    {
        static_assert(Measure::value == 1, "Measure must be 1");
        Update();
        return total_weighted_tardiness;
    }

    /**
     * @brief Returns the quality of the solution.
     * 
     * @return the quality of the solution. 
     */
    double GetQuality() const
    {
        Update();
        return quality;
    }

    /**
     * @brief Inserts in a container the critical blocks of the schedule.
     * 
     * @tparam Iter type of the iterator to be used to insert the critical blocks.
     * @param dest iterator to be used to insert the critical blocks.
     * @return an iterator to the block past the last critical block inserted. 
     */
    template <typename Iter> Iter GetCriticalBlocks(Iter dest) const
    {
        using BlockType = JSPCriticalBlock<ProblemType>;

        Update();

        const auto recursive_critical_blocks = [this](Iter dest, const auto& lambda, const TaskType& current_task, BlockType current_block) -> Iter {
            // insert current task
            current_block.InsertTask(current_task);

            // get previous tasks
            auto [job_predecessor, machine_predecessor] = GetPredecessors(current_task);

            // if there are not any predecessors
            if (!job_predecessor.has_value() && !machine_predecessor.has_value()) {
                *dest++ = current_block;
            }

            // if the job predecessor is critical
            if (job_predecessor.has_value() && EqualTime(GetDuration(*job_predecessor) + heads.at(*job_predecessor), heads.at(current_task))) {
                *dest++ = current_block;
                dest = lambda(dest, lambda, *job_predecessor, BlockType{current_block.GetWeight()});
            }

            // if the machine predecessor is critical (and different from the job predecessor)
            if (machine_predecessor.has_value() && (!job_predecessor.has_value() || job_predecessor->get() != machine_predecessor->get()) &&
                EqualTime(GetDuration(*machine_predecessor) + heads.at(*machine_predecessor), heads.at(current_task))) {
                dest = lambda(dest, lambda, *machine_predecessor, current_block);
            }

            return dest;
        };

        if constexpr (Measure::value == 0) {
            for (const auto& [job, ordered_tasks]: job_order) {
                if (EqualTime(makespan, GetDuration(ordered_tasks.back()) + heads.at(ordered_tasks.back().get()))) {
                    dest = recursive_critical_blocks(dest, recursive_critical_blocks, ordered_tasks.back().get(), BlockType{makespan});
                }
            }
        } else {
            for (const auto& [job, ordered_tasks]: job_order) {
                auto tardiness = heads.at(ordered_tasks.back()) + GetDuration(ordered_tasks.back()) - job.get().GetDueDate();
                if (tardiness > TimeType{}) {
                    dest = recursive_critical_blocks(
                        dest, recursive_critical_blocks, ordered_tasks.back(), BlockType{tardiness * job.get().GetWeight()});
                }
            }
        }

        return dest;
    }

    /**
     * @brief Exchanges the position of two tasks in the same machine.
     * This method only checks that both tasks belong to the same machine,
     * it do not check that the new schedule is feasible.
     * 
     * @param task1 first task.
     * @param task2 second task.
     */
    void ExchangeTasks(const TaskType& task1, const TaskType& task2)
    {
        if (assigned_machine.at(task1).get() != assigned_machine.at(task2).get()) {
            throw std::invalid_argument("Tasks do not belong to the same machine");
        }
        std::swap(machine_order[assigned_machine.at(task1)][machine_position[task1]],
                  machine_order[assigned_machine.at(task2)][machine_position[task2]]);
        std::swap(machine_position[task1], machine_position[task2]);
        changes.insert(task1);
        changes.insert(task2);
        topological_order_changed = true;
    }

    /**
     * @brief Assigns a machine to a task.
     * 
     * @param task task to be considered.
     * @param new_machine machine to be assigned.
     * @param position position of the task in the machine.
     */
    void AssignMachine(const TaskType& task, const MachineType& new_machine, std::size_t position)
    {
        // add changes
        {
            auto [jp, mp] = GetPredecessors(task);
            if (jp.has_value()) {
                changes.insert(*jp);
            }
            if (mp.has_value()) {
                changes.insert(*mp);
            }
            auto [js, ms] = GetSuccessors(task);
            if (js.has_value()) {
                changes.insert(*js);
            }
            if (ms.has_value()) {
                changes.insert(*ms);
            }
        }

        // remove the task from its current machine (if any)
        if (assigned_machine.count(task) != 0) {
            const MachineType& current_machine = assigned_machine.at(task);
            auto& current_machine_tasks = machine_order[current_machine];
            current_machine_tasks.erase(current_machine_tasks.begin() + machine_position.at(task));
            for (std::size_t i = machine_position.at(task); i < current_machine_tasks.size(); i++) {
                machine_position[current_machine_tasks.at(i)]--;
            }
        }

        // insert the task in the new machine
        auto& new_machine_tasks = machine_order[new_machine];
        new_machine_tasks.insert(new_machine_tasks.begin() + position, task);
        for (std::size_t i = position + 1; i < new_machine_tasks.size(); i++) {
            machine_position[new_machine_tasks.at(i)]++;
        }
        machine_position[task] = position;
        assigned_machine.erase(task);
        assigned_machine.emplace(task, new_machine);
        changes.insert(task);
        topological_order_changed = true;

        // add changes
        {
            auto [jp, mp] = GetPredecessors(task);
            if (jp.has_value()) {
                changes.insert(*jp);
            }
            if (mp.has_value()) {
                changes.insert(*mp);
            }
            auto [js, ms] = GetSuccessors(task);
            if (js.has_value()) {
                changes.insert(*js);
            }
            if (ms.has_value()) {
                changes.insert(*ms);
            }
        }
    }

    /**
     * @brief Assigns a machine to a task. The task is inserted in the machine
     * in a feasible position.
     * 
     * @param task task to be considered.
     * @param machine machine to be assigned.
     */
    void AssignMachine(const TaskType& task, const MachineType& machine)
    {
        auto& machine_tasks = machine_order[machine];
        // find position
        auto iter = std::upper_bound(machine_tasks.begin(), machine_tasks.end(), global_position.at(task), [this](const auto& t1, const auto& t2) {
            return t1 < global_position.at(t2);
        });
        auto position = std::distance(machine_tasks.begin(), iter);
        // assign machine
        AssignMachine(task, machine, position);
        topological_order_changed = false;
    }

    /**
     * @brief Applies a move to the solution.
     * 
     * @tparam Move type of the move to be applied.
     * @param move move to be applied.
     */
    template <typename Move> void ApplyMove(const Move& move)
    {
        // apply machine reassignations
        for (const auto& [task, from, to]: move.reassignations) {
            AssignMachine(task, to);
        }

        // apply task inversions
        for (const auto& [from, to]: move.inversions) {
            ExchangeTasks(from, to);
        }
    }

    /**
     * @brief Estimates the quality that results of changing the order
     * of a group of tasks in the same machine.
     * 
     * @tparam Iter type of the iterator to be used to read the group of tasks.
     * @param first iterator pointing to the first task in the new order.
     * @param last iterator pointing to the task past the last task in the new order.
     * @param machine_predecessor task that is scheduled in the same machine before the first task in the group.
     * @param machine_successor task that is scheduled in the same machine after the last task in the group.
     * @return estimate of the quality for the new order of the tasks. 
     */
    template <typename Iter>
    double EstimateQuality(Iter first,
                           Iter last,
                           const std::optional<std::reference_wrapper<const TaskType>>& machine_predecessor,
                           const std::optional<std::reference_wrapper<const TaskType>>& machine_successor) const
    {
        //estimate heads
        robin_hood::unordered_map<std::reference_wrapper<const TaskType>, TimeType, std::hash<TaskType>, std::equal_to<TaskType>> new_heads;
        std::optional<std::reference_wrapper<const TaskType>> job_predecessor = GetJobPredecessor(*first);
        new_heads[*first] =
            std::max(job_predecessor.has_value() ? GetHead(*job_predecessor) + GetDuration(*job_predecessor) : TimeType{},
                     machine_predecessor.has_value() ? GetHead(*machine_predecessor) + GetDuration(*machine_predecessor) : TimeType{});
        for (auto it = std::next(first); it != last; ++it) {
            job_predecessor = GetJobPredecessor(*it);
            new_heads[*it] = std::max(job_predecessor.has_value() ? GetHead(*job_predecessor) + GetDuration(*job_predecessor) : TimeType{},
                                      new_heads[*std::prev(it)] + GetDuration(*std::prev(it)));
        }

        //estimate tails
        robin_hood::unordered_map<
            std::reference_wrapper<const TaskType>,
            robin_hood::unordered_map<std::reference_wrapper<const JobType>, std::optional<TimeType>, std::hash<JobType>, std::equal_to<JobType>>,
            std::hash<TaskType>,
            std::equal_to<TaskType>>
            new_tails;

        const TaskType& last_task = *std::prev(last);
        std::optional<std::reference_wrapper<const TaskType>> job_successor = GetJobSuccessor(last_task);
        for (const auto& [job, tail]: tails.at(last_task)) {
            new_tails[last_task][job] = std::nullopt;
            if (job_successor.has_value() && tails.at(*job_successor).at(job).has_value()) {
                new_tails[last_task][job] = *tails.at(*job_successor).at(job) + GetDuration(*job_successor);
            }
            if (machine_successor.has_value() && tails.at(*machine_successor).at(job).has_value()) {
                new_tails[last_task][job] =
                    new_tails.at(last_task).at(job).has_value()
                        ? std::max(*new_tails.at(last_task).at(job), *tails.at(*machine_successor).at(job) + GetDuration(*machine_successor))
                        : *tails.at(*machine_successor).at(job) + GetDuration(*machine_successor);
            }
        }

        for (auto it = std::prev(last); it-- != first;) {
            job_successor = GetJobSuccessor(*it);
            for (const auto& [job, tail]: tails.at(*it)) {
                new_tails[*it][job] = std::nullopt;
                if (job_successor.has_value() && tails.at(*job_successor).at(job).has_value()) {
                    new_tails[*it][job] = *tails.at(*job_successor).at(job) + GetDuration(*job_successor);
                }
                if (new_tails.at(*std::next(it)).at(job).has_value()) {
                    new_tails[*it][job] =
                        new_tails.at(*it).at(job).has_value()
                            ? std::max(*new_tails.at(*it).at(job), *new_tails.at(*std::next(it)).at(job) + GetDuration(*std::next(it)))
                            : *new_tails.at(*std::next(it)).at(job) + GetDuration(*std::next(it));
                }
            }
        }

        // calculate measures
        if constexpr (Measure::value == 0) {
            TimeType mks{};
            for (auto it = first; it != last; ++it) {
                mks = std::max(mks, new_heads[*it] + GetDuration(*it) + *new_tails[*it][dummy]);
            }
            return 1.0 / (double)mks;
        } else {
            std::optional<TimeType> completion_time = std::nullopt;
            TimeType twt{};
            for (const auto& [job, ordered_tasks]: job_order) {
                for (auto it = first; it != last; ++it) {
                    if (new_tails.at(*it).at(job).has_value()) {
                        completion_time = std::max(completion_time.value_or(), new_heads.at(*it) + GetDuration(*it) + new_tails.at(*it).at(job));
                    }
                }
                auto tardiness =
                    (completion_time.has_value() ? completion_time : (heads.at(ordered_tasks.back()) + GetDuration(ordered_tasks.back()))) -
                    job.get().GetDueDate();

                twt += std::max(TimeType{}, tardiness) * job.get().GetWeight();
            }

            if (twt == TimeType{}) {
                return std::numeric_limits<double>::max();
            } else {
                return 1.0 / (double)twt;
            }
        }
    }

    /**
     * @brief Estimates the quality that results of changing the order
     * of two contiguous tasks.
     * 
     * @param task1 first task.
     * @param task2 second task.
     * @return estimate of the quality for the new order of the tasks. 
     */
    double EstimateQuality(const TaskType& task1, const TaskType& task2) const
    {
        auto task1_job_predecessor = GetJobPredecessor(task1);
        auto task2_job_predecessor = GetJobPredecessor(task2);
        auto task1_job_successor = GetJobSuccessor(task1);
        auto task2_job_successor = GetJobSuccessor(task2);
        auto task1_machine_predecessor = GetMachinePredecessor(task1);
        auto task2_machine_successor = GetMachineSuccessor(task2);

        auto task2_head = std::max(
            task2_job_predecessor.has_value() ? GetHead(*task2_job_predecessor) + GetDuration(*task2_job_predecessor) : TimeType{},
            task1_machine_predecessor.has_value() ? GetHead(*task1_machine_predecessor) + GetDuration(*task1_machine_predecessor) : TimeType{});

        auto task1_head =
            std::max(task1_job_predecessor.has_value() ? GetHead(*task1_job_predecessor) + GetDuration(*task1_job_predecessor) : TimeType{},
                     task2_head + GetDuration(task2));

        // calculate measures
        if constexpr (Measure::value == 0) {
            auto task1_tail = std::max(
                task1_job_successor.has_value() ? GetTail(*task1_job_successor) + GetDuration(*task1_job_successor) : TimeType{},
                task2_machine_successor.has_value() ? GetTail(*task2_machine_successor) + GetDuration(*task2_machine_successor) : TimeType{});
            auto task2_tail =
                std::max(task2_job_successor.has_value() ? GetTail(*task2_job_successor) + GetDuration(*task2_job_successor) : TimeType{},
                         task1_tail + GetDuration(task1));

            auto task1_mks = task1_head + GetDuration(task1) + task1_tail;
            auto task2_mks = task2_head + GetDuration(task2) + task2_tail;

            return 1.0 / (double)std::max(task1_mks, task2_mks);
        } else {
            TimeType twt{};
            for (const auto& [job, ordered_tasks]: job_order) {
                if (GetTail(task1, job).has_value() || GetTail(task2, job).has_value()) {
                    std::optional<TimeType> task1_tail = std::nullopt;
                    if (task1_job_successor.has_value() && GetTail(*task1_job_successor, job).has_value()) {
                        task1_tail = *GetTail(*task1_job_successor, job) + GetDuration(*task1_job_successor);
                    }
                    if (task2_machine_successor.has_value() && GetTail(*task2_machine_successor, job).has_value()) {
                        task1_tail = task1_tail.has_value()
                                         ? std::max(*task1_tail, *GetTail(*task2_machine_successor, job) + GetDuration(*task2_machine_successor))
                                         : *GetTail(*task2_machine_successor, job) + GetDuration(*task2_machine_successor);
                    }
                    std::optional<TimeType> task2_tail = task1_tail.has_value() ? std::make_optional(*task1_tail + GetDuration(task1)) : std::nullopt;
                    if (task2_job_successor.has_value() && GetTail(*task2_job_successor, job).has_value()) {
                        task2_tail = task2_tail.has_value()
                                         ? std::max(*task2_tail, *GetTail(*task2_job_successor, job) + GetDuration(*task2_job_successor))
                                         : *GetTail(*task2_job_successor, job) + GetDuration(*task2_job_successor);
                    }

                    auto task1_tardiness = task1_tail.has_value()
                                               ? std::max(task1_head + GetDuration(task1) + *task1_tail - job.get().GetDueDate(), TimeType{})
                                               : TimeType{};
                    auto task2_tardiness = task2_tail.has_value()
                                               ? std::max(task2_head + GetDuration(task2) + *task2_tail - job.get().GetDueDate(), TimeType{})
                                               : TimeType{};

                    twt += std::max(task1_tardiness, task2_tardiness) * job.get().GetWeight();
                } else {
                    twt += std::max((GetHead(ordered_tasks.back()) + GetDuration(ordered_tasks.back()) - job.get().GetDueDate()), TimeType{}) *
                           job.get().GetWeight();
                }
            }
            if (twt == TimeType{}) {
                return std::numeric_limits<double>::max();
            } else {
                return 1.0 / (double)twt;
            }
        }
    }

    /**
     * @brief Estimates the quality of the solution result of a machine reassignation.
     * 
     * @param task task whose machine will be reassigned.
     * @param new_machine machine to be assigned.
     * @return the estimated quality of the new solution. 
     */
    double EstimateQuality(const TaskType& task, const MachineType& new_machine)
    {
        auto& machine_tasks = machine_order[new_machine];
        auto iter = std::upper_bound(machine_tasks.begin(), machine_tasks.end(), global_position.at(task), [this](const auto& t1, const auto& t2) {
            return t1 < global_position.at(t2);
        });
        std::size_t position = std::distance(machine_tasks.begin(), iter);

        auto machine_predecessor = position > 0 ? std::make_optional(machine_order.at(new_machine).at(position - 1)) : std::nullopt;
        auto machine_successor =
            position < machine_order.at(new_machine).size() ? std::make_optional(machine_order.at(new_machine).at(position)) : std::nullopt;
        auto job_predecessor = GetJobPredecessor(task);
        auto job_successor = GetJobSuccessor(task);
        auto head = std::max(job_predecessor.has_value() ? GetHead(*job_predecessor) + GetDuration(*job_predecessor) : TimeType{},
                             machine_predecessor.has_value() ? GetHead(*machine_predecessor) + GetDuration(*machine_predecessor) : TimeType{});

        // calculate measures
        if constexpr (Measure::value == 0) {
            auto tail = std::max(job_successor.has_value() ? GetTail(*job_successor) + GetDuration(*job_successor) : TimeType{},
                                 machine_successor.has_value() ? GetTail(*machine_successor) + GetDuration(*machine_successor) : TimeType{});
            auto quality = head + task.GetDuration(new_machine) + tail;
            return 1.0 / (double)quality;
        } else {
            TimeType twt{};
            for (const auto& [job, ordered_tasks]: job_order) {
                std::optional<TimeType> tail = std::nullopt;
                if (job_successor.has_value() && GetTail(*job_successor, job).has_value()) {
                    tail = *GetTail(*job_successor, job) + GetDuration(*job_successor);
                }
                if (machine_successor.has_value() && GetTail(*machine_successor, job).has_value()) {
                    tail = tail.has_value() ? std::max(*tail, *GetTail(*machine_successor, job) + GetDuration(*machine_successor))
                                            : *GetTail(*machine_successor, job) + GetDuration(*machine_successor);
                }

                if (tail.has_value()) {
                    twt += std::max(head + task.GetDuration(new_machine) + *tail - job.get().GetDueDate(), TimeType{}) * job.get().GetWeight();
                } else {
                    twt += std::max((GetHead(ordered_tasks.back()) + GetDuration(ordered_tasks.back()) - job.get().GetDueDate()), TimeType{}) *
                           job.get().GetWeight();
                }
            }
            if (twt == TimeType{}) {
                return std::numeric_limits<double>::max();
            } else {
                return 1.0 / (double)twt;
            }
        }
    }

    /**
     * @brief Calculates the quality of the solution result of applying
     * a move.
     * 
     * @tparam Move type of the move to be applied.
     * @param move move to be applied.
     */
    template <typename Move> double GetMoveQuality(Move& move)
    {
        // make a copy
        JSPRegularMeasuresMinimizationSolution copy = *this;
        // start simulation
        copy.ApplyMove(move); // apply the move
        copy.UpdateTopologicalOrder(); // update topological order
        copy.UpdateHeads(); // update the heads
        if constexpr (Measure::value == 0) {
            copy.UpdateMakespan();
            return 1.0 / (double)copy.makespan;
        } else {
            copy.UpdateTotalWeightedTardiness();
            if (copy.total_weighted_tardiness == TimeType{}) {
                return std::numeric_limits<double>::max();
            } else {
                return 1.0 / (double)copy.total_weighted_tardiness;
            }
        }
    }

    /**
     * @brief Inserts in a container all the tasks.
     * 
     * @tparam Iter type of the iterator to be used to insert the tasks.
     * @param dest iterator to be used to insert the tasks.
     * @return an iterator to the task past the last task inserted. 
     */
    template <typename Iter> Iter GetTasks(Iter dest) const
    {
        return GetProblem().GetTasks(dest);
    }

    /**
     * @brief Inserts in a container all the tasks in topological order.
     * 
     * @tparam Iter type of the iterator to be used to insert the tasks.
     * @param dest iterator to be used to insert the tasks.
     * @return an iterator to the task past the last task inserted. 
     */
    template <typename Iter> Iter GetTasksTopologicalOrder(Iter dest) const
    {
        return std::copy(processing_order.begin(), processing_order.end(), dest);
    }

    /**
     * @brief Returns a pair with the tasks that are scheduled before the specified task. The first component is
     * the job predecessor and the second component the machine predecessor.
     * 
     * @param task task whose previous tasks will be returned.
     * @return a pair with the tasks that are scheduled before the specified task.
     */
    std::pair<std::optional<std::reference_wrapper<const TaskType>>, std::optional<std::reference_wrapper<const TaskType>>>
    GetPredecessors(const TaskType& task) const
    {
        return std::make_pair(GetJobPredecessor(task), GetMachinePredecessor(task));
    }

    /**
     * @brief Returns a pair with the tasks that are scheduled after the specified task. The first component is
     * the job successor and the second component the machine successor.
     * 
     * @param task task whose following tasks will be returned.
     * @return a pair with the tasks that are scheduled after the specified task.
     */
    std::pair<std::optional<std::reference_wrapper<const TaskType>>, std::optional<std::reference_wrapper<const TaskType>>>
    GetSuccessors(const TaskType& task) const
    {
        return std::make_pair(GetJobSuccessor(task), GetMachineSuccessor(task));
    }

    /**
     * @brief Returns the task that is scheduled before in the same job as the specified task.
     * 
     * @param task task whose previous task will be returned.
     * @return the task that is scheduled before in the same job as the specified task.
     */
    std::optional<std::reference_wrapper<const TaskType>> GetJobPredecessor(const TaskType& task) const
    {
        return job_position.at(task) > 0 ? std::make_optional(job_order.at(task.GetJob()).at(job_position.at(task) - 1)) : std::nullopt;
    }

    /**
     * @brief Returns the task that is scheduled after in the same job as the specified task.
     * 
     * @param task task whose following task will be returned.
     * @return the task that is scheduled after in the same job as the specified task.
     */
    std::optional<std::reference_wrapper<const TaskType>> GetJobSuccessor(const TaskType& task) const
    {
        return job_position.at(task) < job_order.at(task.GetJob()).size() - 1
                   ? std::make_optional(job_order.at(task.GetJob()).at(job_position.at(task) + 1))
                   : std::nullopt;
    }

    /**
     * @brief Returns the task that is scheduled before in the same machine as the specified task.
     * 
     * @param task task whose previous task will be returned.
     * @return the task that is scheduled before in the same machine as the specified task.
     */
    std::optional<std::reference_wrapper<const TaskType>> GetMachinePredecessor(const TaskType& task) const
    {
        return machine_position.at(task) > 0 ? std::make_optional(machine_order.at(assigned_machine.at(task)).at(machine_position.at(task) - 1))
                                             : std::nullopt;
    }

    /**
     * @brief Returns the task that is scheduled after in the same machine as the specified task.
     * 
     * @param task task whose following task will be returned.
     * @return the task that is scheduled after in the same machine as the specified task.
     */
    std::optional<std::reference_wrapper<const TaskType>> GetMachineSuccessor(const TaskType& task) const
    {
        return machine_position.at(task) < machine_order.at(assigned_machine.at(task)).size() - 1
                   ? std::make_optional(machine_order.at(assigned_machine.at(task)).at(machine_position.at(task) + 1))
                   : std::nullopt;
    }

    /**
     * @brief Inserts in a container the initial tasks.
     * 
     * @tparam Iter type of the iterator to be used to insert the tasks.
     * @param dest iterator to be used to insert the tasks.
     * @return an iterator to the task past the last task inserted. 
     */
    template <typename Iter> Iter GetInitialTasks(Iter dest) const
    {
        return GetProblem().GetInitialTasks(dest);
    }

    /**
     * @brief Inserts in a container the final tasks.
     * 
     * @tparam Iter type of the iterator to be used to insert the tasks.
     * @param dest iterator to be used to insert the tasks.
     * @return an iterator to the task past the last task inserted. 
     */
    template <typename Iter> Iter GetFinalTasks(Iter dest) const
    {
        return GetProblem().GetFinalTasks(dest);
    }

    /**
     * @brief Returns the head of the specified task.
     * 
     * @param task task whose head will be returned.
     * @return the head of the specified task.
     */
    TimeType GetHead(const TaskType& task) const
    {
        Update();
        return heads.at(task);
    }

    /**
     * @brief Returns the tail of the specified task.
     * 
     * @param task task whose tail will be returned.
     * @return the tail of the specified task.
     */
    TimeType GetTail(const TaskType& task) const
    {
        static_assert(Tails::value, "GetTail is only available when template parameter Tails is set to true");
        static_assert(Measure::value == 0, "Measure must be 0");
        Update();
        return *tails.at(task).at(dummy);
    }

    /**
     * @brief Returns the tail of the specified task for the specified job.
     * 
     * @param task task whose tail will be returned.
     * @param job job to be considered.
     * @return the tail of the specified task for the specified job. 
     */
    std::optional<TimeType> GetTail(const TaskType& task, const JobType& job) const
    {
        static_assert(Tails::value, "GetTail is only available when template parameter Tails is set to true");
        static_assert(Measure::value == 1, "Measure must be 1");
        Update();
        return tails.at(task).at(job);
    }

    /**
     * @brief Returns the machine assigned to a task.
     * 
     * @param task task whose assigned machine will be returned.
     * @return machine assigned to the task.
     */
    const MachineType& GetAssignedMachine(const TaskType& task) const
    {
        return assigned_machine.at(task);
    }

    /**
     * @brief Returns the duration of a task in the current assigned machine.
     * 
     * @param task task whose duration will be returned.
     * @return the duration of the task in the current assigned machine. 
     */
    TimeType GetDuration(const TaskType& task) const
    {
        return task.GetDuration(assigned_machine.at(task));
    }

    /**
     * @brief Returns the problem that the solution solves.
     * 
     * @return the problem that the solution solves.
     */
    const ProblemType& GetProblem() const
    {
        return problem;
    }

    /**
     * @brief Returns a string with the solution.
     * 
     * @return a string with the solution.
     */
    std::string SolutionSequence() const
    {
        std::string solution;
        std::vector<std::reference_wrapper<const MachineType>> machines;
        GetProblem().GetMachines(std::back_inserter(machines));
        std::sort(
            machines.begin(), machines.end(), [](const MachineType& m1, const MachineType& m2) { return m1.GetMachineID() < m2.GetMachineID(); });
        for (const MachineType& machine: machines) {
            for (const TaskType& task: machine_order.at(machine)) {
                solution += std::to_string(task.GetJob().GetJobID()) + "-" + std::to_string(task.GetPosition()) + " ";
            }
            solution += "\n";
        }
        return solution;
    }

    /**
     * @brief Returns the optimizing function.
     * 
     * @return 0 makespan, 1 total weighted tardiness.    
     */
    constexpr static bool GetMeasure()
    {
        return Measure::value;
    }

    /**
     * @brief Checks if the solution has tails enabled.
     * 
     * @return true if the solution has tails enabled, false in other case.    
     */
    constexpr static bool HasTails()
    {
        return Tails::value;
    }

    /**
     * @brief Returns a string representing the solution.
     * 
     * @return a string representing the solution.
     */
    inline std::string ToString() const
    {
        std::stringstream ss;
        if constexpr (Measure::value == 0) {
            ss << "Makespan " << GetMakespan() << "\n" << SolutionSequence();
        } else {
            ss << "Total Weighted Tardiness " << GetTotalWeightedTardiness() << "\n" << SolutionSequence();
        }
        return ss.str();
    }

    bool operator==(const JSPRegularMeasuresMinimizationSolution& other) const
    {
        return job_order == other.job_order && machine_order == other.machine_order && assigned_machine == other.assigned_machine;
    };

    bool operator!=(const JSPRegularMeasuresMinimizationSolution& other) const
    {
        return job_order != other.job_order || machine_order != other.machine_order || assigned_machine != other.assigned_machine;
    };

    bool operator<(const JSPRegularMeasuresMinimizationSolution& other) const
    {
        return GetQuality() < other.GetQuality();
    };

    bool operator>(const JSPRegularMeasuresMinimizationSolution& other) const
    {
        return GetQuality() > other.GetQuality();
    };

    bool operator<=(const JSPRegularMeasuresMinimizationSolution& other) const
    {
        return GetQuality() <= other.GetQuality();
    };

    bool operator>=(const JSPRegularMeasuresMinimizationSolution& other) const
    {
        return GetQuality() >= other.GetQuality();
    };

    friend std::ostream& operator<<(std::ostream& os, const JSPRegularMeasuresMinimizationSolution& sol)
    {
        return os << sol.ToString();
    }
};

#endif /* JSPREGULARMEASURESMINIMIZATIONSOLUTION_HPP_ */
