/**
 * @file jsp_move.hpp
 * @author Pablo
 * @brief JSP Move.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef JSPMOVE_HPP_
#define JSPMOVE_HPP_

#include <vector>

/**
 * @brief Move in a JSP.
 * 
 * @tparam Problem type of the problem.
 */
template <typename Problem> class JSPMove
{
  public:
    friend class std::hash<JSPMove>;
    using ProblemType = Problem;
    using TaskType = typename ProblemType::TaskType;
    using MachineType = typename ProblemType::MachineType;

    std::vector<std::pair<std::reference_wrapper<const TaskType>, std::reference_wrapper<const TaskType>>> inversions;
    std::vector<
        std::tuple<std::reference_wrapper<const TaskType>, std::reference_wrapper<const MachineType>, std::reference_wrapper<const MachineType>>>
        reassignations;

    /**
     * @brief Adds a inversion of the order of two tasks.
     * 
     * @param from task that is scheduled before and will be scheduled after.
     * @param to task that is scheduled after and will be scheduled before.
     */
    void AddInversion(const TaskType& from, const TaskType& to)
    {
        inversions.push_back(std::make_pair(std::cref(from), std::cref(to)));
    }

    /**
     * @brief Adds a reassignation of the machine of a task.
     * 
     * @param task task whose machine will be reassigned.
     * @param from current machine.
     * @param to new machine.
     */
    void AddReassignation(const TaskType& task, const MachineType& from, const MachineType& to)
    {
        reassignations.push_back(std::make_tuple(std::cref(task), std::cref(from), std::cref(to)));
    }

    /**
     * @brief Inverts the move.
     * 
     * @return this move inverted.
     */
    JSPMove& Invert()
    {
        std::for_each(inversions.begin(), inversions.end(), [](auto& pair) { std::swap(pair.first, pair.second); });
        std::reverse(inversions.begin(), inversions.end());
        std::for_each(reassignations.begin(), reassignations.end(), [](auto& tuple) { std::swap(std::get<1>(tuple), std::get<2>(tuple)); });
        std::reverse(reassignations.begin(), reassignations.end());
        return *this;
    }

    /**
     * @brief Inserts in a container all the inversions.
     * 
     * @tparam Iter type of the iterator to be used to insert the inversions.
     * @param dest iterator to be used to insert the inversions.
     * @return an iterator to the inversions past the last inversions inserted. 
     */
    template <typename Iter> Iter GetInversions(Iter dest) const
    {
        return std::copy(inversions.begin(), inversions.end(), dest);
    }

    /**
     * @brief Inserts in a container all the reassignations.
     * 
     * @tparam Iter type of the iterator to be used to insert the reassignations.
     * @param dest iterator to be used to insert the reassignations.
     * @return an iterator to the reassignation past the last reassignation inserted. 
     */
    template <typename Iter> Iter GetReassignations(Iter dest) const
    {
        return std::copy(reassignations.begin(), reassignations.end(), dest);
    }

    bool operator==(const JSPMove& other) const
    {
        return distance(inversions.begin(), inversions.end()) == distance(other.inversions.begin(), other.inversions.end()) &&
               equal(inversions.begin(), inversions.end(), other.inversions.begin(), std::equal_to<std::pair<TaskType, TaskType>>()) &&
               distance(reassignations.begin(), reassignations.end()) == distance(other.reassignations.begin(), other.reassignations.end()) &&
               equal(reassignations.begin(),
                     reassignations.end(),
                     other.reassignations.begin(),
                     std::equal_to<std::tuple<TaskType, MachineType, MachineType>>());
    }

    bool operator!=(const JSPMove& other) const
    {
        return distance(inversions.begin(), inversions.end()) != distance(other.inversions.begin(), other.inversions.end()) ||
               !equal(inversions.begin(), inversions.end(), other.inversions.begin(), std::equal_to<std::pair<TaskType, TaskType>>()) ||
               distance(reassignations.begin(), reassignations.end()) != distance(other.reassignations.begin(), other.reassignations.end()) ||
               !equal(reassignations.begin(),
                      reassignations.end(),
                      other.reassignations.begin(),
                      std::equal_to<std::tuple<TaskType, MachineType, MachineType>>());
    }
};

namespace std
{
    template <typename Problem> struct hash<JSPMove<Problem>>
    {
        size_t operator()(const JSPMove<Problem>& k) const
        {
            std::size_t seed = k.inversions.size() + k.reassignations.size();
            for (auto& e: k.inversions) {
                seed ^= std::hash<typename JSPMove<Problem>::TaskType>{}(e.first) ^
                        (std::hash<typename JSPMove<Problem>::TaskType>{}(e.second) + 0x9e3779b9 + (seed << 6) + (seed >> 2));
            }
            for (auto& e: k.reassignations) {
                seed ^= std::hash<typename JSPMove<Problem>::TaskType>{}(std::get<0>(e)) ^
                        std::hash<typename JSPMove<Problem>::MachineType>{}(std::get<1>(e)) ^
                        (std::hash<typename JSPMove<Problem>::MachineType>{}(std::get<2>(e)) + 0x9e3779b9 + (seed << 6) + (seed >> 2));
            }
            return seed;
        }
    };
}

#endif /* JSPMOVE_HPP_ */
