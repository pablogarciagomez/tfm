/**
 * @file jsp_neighborhoods.hpp
 * @author Pablo
 * @brief JSP Neighborhoods.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef JSPNEIGHBORHOODS_HPP_
#define JSPNEIGHBORHOODS_HPP_

#include <algorithm>
#include <type_traits>
#include <utility>
#include <vector>

#include <problems/jsp/jsp_critical_block.hpp>
#include <problems/jsp/jsp_move.hpp>
#include <problems/jsp/jsp_regular_measures_minimization_solution.hpp>
#include <utils/template_utils.hpp>

/**
 * @brief CET Neighborhood for JSP.
 * 
 * @tparam Problem JSP.
 * @tparam Move type of the moves to be returned.
 * @tparam Estimate if true an estimate will be used.
 */
template <typename Problem, template <typename> class Move = JSPMove, typename Estimate = std::false_type> class CET
{
  public:
    using ProblemType = Problem;
    using TaskType = typename ProblemType::TaskType;
    using MoveType = Move<Problem>;

    /**
     * @brief Calculates the CET neighbors of a solution from the given critical blocks.
     * 
     * @tparam InputIt type of the iterator to be used to read the critical blocks.
     * @tparam OutputIt type of the iterator to be used to insert the neighbors.
     * @tparam Solution type of the solution.
     * @param first iterator pointing to the first critical block to be considered.
     * @param last iterator pointing to the critical block past the last critical block to be considered.
     * @param dest iterator to be used to insert the neighbors.
     * @param solution solution whose neighbors will be calculated.
     * @return an iterator to the neighbor past the last neighbor inserted.  
     */
    template <typename InputIt, typename OutputIt, typename Solution>
    static OutputIt CalculateNeighbors(InputIt first, InputIt last, OutputIt dest, Solution solution)
    {
        for (auto block = first; block != last; ++block) {
            std::vector<std::reference_wrapper<const TaskType>> block_tasks;
            block->GetTasks(std::back_inserter(block_tasks));
            if (block_tasks.size() > 1) {
                MoveType move;
                move.AddInversion(block_tasks.at(0), block_tasks.at(1));
                *dest++ = MoveData(move, GetQuality(solution, block_tasks.at(0), block_tasks.at(1)), UsesEstimates());
            }
            if (block_tasks.size() > 2) {
                MoveType move;
                move.AddInversion(block_tasks.at(block_tasks.size() - 2), block_tasks.at(block_tasks.size() - 1));
                *dest++ = MoveData(
                    move, GetQuality(solution, block_tasks.at(block_tasks.size() - 2), block_tasks.at(block_tasks.size() - 1)), UsesEstimates());
            }
        }
        return dest;
    }

    /**
     * @brief Inserts in a container the CET neighbors of a solution.
     * 
     * @tparam Iter type of the iterator to be used to insert the neighbors.
     * @tparam Solution type of the solution whose neighbors will be calculated.
     * @param dest iterator to be used to insert the neighbors.
     * @param solution solution whose neighbors will be calculated.
     * @return an iterator to the neighbor past the last neighbor inserted. 
     */
    template <typename Iter, typename Solution> static Iter GetNeighbors(Iter dest, const Solution& solution)
    {
        std::unordered_set<JSPCriticalBlock<ProblemType>, std::hash<JSPCriticalBlock<ProblemType>>, std::equal_to<JSPCriticalBlock<ProblemType>>>
            critical_blocks;
        solution.GetCriticalBlocks(std::inserter(critical_blocks, critical_blocks.begin()));
        return CalculateNeighbors(critical_blocks.begin(), critical_blocks.end(), dest, solution);
    }

    /**
     * @brief Estimates the quality that results of inverting two tasks.
     * 
     * @tparam Solution type of the solution. 
     * @param solution the solution to be considered.
     * @param task1 first task.
     * @param task2 second task.
     * @return the quality that results of inverting two tasks.
     */
    template <typename Solution> static double GetQuality(Solution& solution, const TaskType& task1, const TaskType& task2)
    {
        if constexpr (Estimate::value) {
            return solution.EstimateQuality(task1, task2);
        } else {
            solution.ExchangeTasks(task1, task2);
            auto quality = solution.GetQuality();
            solution.ExchangeTasks(task2, task1);
            return quality;
        }
    }

    /**
     * @brief Checks if the solution uses estimates.
     * 
     * @return true if the solution uses estimates, false in other case.  
     */
    constexpr static bool UsesEstimates()
    {
        return Estimate::value;
    }
};

/**
 * @brief CEI Neighborhood for JSP.
 * 
 * @tparam Problem JSP.
 * @tparam Move type of the moves to be returned.
 * @tparam Estimate if true an estimate will be used.
 */
template <typename Problem, template <typename> class Move = JSPMove, typename Estimate = std::false_type> class CEI
{
  public:
    using ProblemType = Problem;
    using TaskType = typename ProblemType::TaskType;
    using TimeType = typename ProblemType::TimeType;
    using MoveType = Move<Problem>;

    /**
     * @brief Calculates the CEI neighbors of a solution from the given critical blocks.
     * 
     * @tparam InputIt type of the iterator to be used to read the critical blocks.
     * @tparam OutputIt type of the iterator to be used to insert the neighbors.
     * @tparam Solution type of the solution.
     * @param first iterator pointing to the first critical block to be considered.
     * @param last iterator pointing to the critical block past the last critical block to be considered.
     * @param dest iterator to be used to insert the neighbors.
     * @param solution solution whose neighbors will be calculated.
     * @return an iterator to the neighbor past the last neighbor inserted.  
     */
    template <typename InputIt, typename OutputIt, typename Solution>
    static OutputIt CalculateNeighbors(InputIt first, InputIt last, OutputIt dest, Solution solution)
    {
        for (auto block = first; block != last; ++block) {
            std::vector<std::reference_wrapper<const TaskType>> block_tasks;
            block->GetTasks(std::back_inserter(block_tasks));
            // shift the operations at the end
            for (auto it1 = block_tasks.begin(); it1 != block_tasks.end(); ++it1) {
                MoveType move;
                std::deque<std::reference_wrapper<const TaskType>> new_order;
                auto successor = solution.GetJobSuccessor(*it1);
                auto completion_time = successor.has_value() ? solution.GetHead(successor.value()) +
                                                                   successor.value().get().GetDuration(solution.GetAssignedMachine(successor.value()))
                                                             : TimeType{};

                for (auto it2 = it1 + 1; it2 != block_tasks.end(); ++it2) {
                    auto predecessor = solution.GetJobPredecessor(*it2);
                    auto head = predecessor.has_value() ? solution.GetHead(predecessor.value()) : TimeType{};
                    if (head >= completion_time) {
                        break;
                    }
                    move.AddInversion(*it1, *it2);
                    new_order.push_back(*it2);
                }
                if (!new_order.empty()) {
                    new_order.push_back(*it1);
                    *dest++ = MoveData(move,
                                       GetQuality(solution,
                                                  move,
                                                  new_order.begin(),
                                                  new_order.end(),
                                                  solution.GetMachinePredecessor(new_order.at(new_order.size() - 1)),
                                                  solution.GetMachineSuccessor(new_order.at(new_order.size() - 2))),
                                       UsesEstimates());
                }
            }
            // shift the operations at the beginning
            for (auto it1 = block_tasks.rbegin(); it1 != block_tasks.rend(); ++it1) {
                MoveType move;
                std::deque<std::reference_wrapper<const TaskType>> new_order;
                auto predecessor = solution.GetJobPredecessor(*it1);
                auto head = predecessor.has_value() ? solution.GetHead(predecessor.value()) : TimeType{};

                for (auto it2 = it1 + 1; it2 != block_tasks.rend(); ++it2) {
                    auto successor = solution.GetJobSuccessor(*it2);
                    auto completion_time = successor.has_value()
                                               ? solution.GetHead(successor.value()) +
                                                     successor.value().get().GetDuration(solution.GetAssignedMachine(successor.value()))
                                               : TimeType{};
                    if (head >= completion_time) {
                        break;
                    }
                    move.AddInversion(*it2, *it1);
                    new_order.push_front(*it2);
                }
                if (!new_order.empty()) {
                    new_order.push_front(*it1);
                    *dest++ = MoveData(move,
                                       GetQuality(solution,
                                                  move,
                                                  new_order.begin(),
                                                  new_order.end(),
                                                  solution.GetMachinePredecessor(new_order.at(1)),
                                                  solution.GetMachineSuccessor(new_order.at(0))),
                                       UsesEstimates());
                }
            }
        }
        return dest;
    }

    /**
     * @brief Inserts in a container the CEI neighbors of a solution.
     * 
     * @tparam Iter type of the iterator to be used to insert the neighbors.
     * @tparam Solution type of the solution whose neighbors will be calculated.
     * @param dest iterator to be used to insert the neighbors.
     * @param solution solution whose neighbors will be calculated.
     * @return an iterator to the neighbor past the last neighbor inserted. 
     */
    template <typename Iter, typename Solution> static Iter GetNeighbors(Iter dest, const Solution& solution)
    {
        std::unordered_set<JSPCriticalBlock<ProblemType>, std::hash<JSPCriticalBlock<ProblemType>>, std::equal_to<JSPCriticalBlock<ProblemType>>>
            critical_blocks;
        solution.GetCriticalBlocks(std::inserter(critical_blocks, critical_blocks.begin()));
        return CalculateNeighbors(critical_blocks.begin(), critical_blocks.end(), dest, solution);
    }

    /**
     * @brief Returns the quality that results of changing the order
     * of a group of tasks in the same machine.
     * 
     * @tparam Solution type of the solution.
     * @tparam Iter type of the iterator to be used to read the group of tasks.
     * @param solution solution to be considered.
     * @param move move to be applied.
     * @param first iterator pointing to the first task in the new order.
     * @param last iterator pointing to the task past the last task in the new order.
     * @param before task that is scheduled in the same machine before the first task in the group.
     * @param after task that is scheduled in the same machine after the last task in the group.
     * @return quality for the new order of the tasks.  
     */
    template <typename Solution, typename Iter>
    static double GetQuality(Solution& solution,
                             MoveType& move,
                             [[maybe_unused]] Iter first,
                             [[maybe_unused]] Iter last,
                             const std::optional<std::reference_wrapper<const typename Solution::TaskType>>& before,
                             const std::optional<std::reference_wrapper<const typename Solution::TaskType>>& after)
    {
        if constexpr (Estimate::value) {
            if constexpr (solution.GetMeasure() == 0) {
                return solution.EstimateQuality(first, last, before, after);
            } else if constexpr (solution.GetMeasure() == 1) {
                return solution.EstimateQuality(first, last, before, after);
            }
        } else {
            solution.ApplyMove(move);
            auto quality = solution.GetQuality();
            solution.ApplyMove(move.Invert());
            move.Invert();
            return quality;
        }
    }

    /**
     * @brief Checks if the solution uses estimates.
     * 
     * @return true if the solution uses estimates, false in other case.  
     */
    constexpr static bool UsesEstimates()
    {
        return Estimate::value;
    }
};

/**
 * @brief MachineReassignation Neighborhood for JSP.
 * 
 * @tparam Problem JSP.
 * @tparam Move type of the moves to be returned.
 * @tparam Estimate if true an estimate will be used.
 */
template <typename Problem, template <typename> class Move = JSPMove, typename Estimate = std::false_type> class MachineReassignation
{
  public:
    using ProblemType = Problem;
    using TaskType = typename ProblemType::TaskType;
    using MachineType = typename ProblemType::MachineType;
    using TimeType = typename ProblemType::TimeType;
    using MoveType = Move<Problem>;

    /**
     * @brief Calculates the MachineReassignation neighbors of a solution from the given critical blocks.
     * 
     * @tparam InputIt type of the iterator to be used to read the critical blocks.
     * @tparam OutputIt type of the iterator to be used to insert the neighbors.
     * @tparam Solution type of the solution.
     * @param first iterator pointing to the first critical block to be considered.
     * @param last iterator pointing to the critical block past the last critical block to be considered.
     * @param dest iterator to be used to insert the neighbors.
     * @param solution solution whose neighbors will be calculated.
     * @return an iterator to the neighbor past the last neighbor inserted.  
     */
    template <typename InputIt, typename OutputIt, typename Solution>
    static OutputIt CalculateNeighbors(InputIt first, InputIt last, OutputIt dest, Solution solution)
    {
        // calculate critical tasks
        std::unordered_set<std::reference_wrapper<const TaskType>, std::hash<TaskType>, std::equal_to<TaskType>> critical_tasks;
        for (auto block = first; block != last; ++block) {
            block->GetTasks(std::inserter(critical_tasks, critical_tasks.begin()));
        }
        // reassign critical tasks
        for (const TaskType& task: critical_tasks) {
            std::vector<std::reference_wrapper<const MachineType>> machines;
            task.GetMachines(std::back_inserter(machines));
            const MachineType& current_machine = solution.GetAssignedMachine(task);
            for (const MachineType& machine: machines) {
                if (current_machine != machine) {
                    MoveType move{};
                    move.AddReassignation(task, current_machine, machine);
                    if constexpr (Estimate::value) {
                        *dest++ = MoveData(move, solution.EstimateQuality(task, machine), UsesEstimates());
                    } else {
                        solution.AssignMachine(task, machine);
                        auto quality = solution.GetQuality();
                        solution.AssignMachine(task, current_machine);
                        *dest++ = MoveData(move, quality, UsesEstimates());
                    }
                }
            }
        }

        return dest;
    }

    /**
     * @brief Inserts in a container the MachineReassignation neighbors of a solution.
     * 
     * @tparam Iter type of the iterator to be used to insert the neighbors.
     * @tparam Solution type of the solution whose neighbors will be calculated.
     * @param dest iterator to be used to insert the neighbors.
     * @param solution solution whose neighbors will be calculated.
     * @return an iterator to the neighbor past the last neighbor inserted. 
     */
    template <typename Iter, typename Solution> static Iter GetNeighbors(Iter dest, const Solution& solution)
    {
        std::unordered_set<JSPCriticalBlock<ProblemType>, std::hash<JSPCriticalBlock<ProblemType>>, std::equal_to<JSPCriticalBlock<ProblemType>>>
            critical_blocks;
        solution.GetCriticalBlocks(std::inserter(critical_blocks, critical_blocks.begin()));
        return CalculateNeighbors(critical_blocks.begin(), critical_blocks.end(), dest, solution);
    }

    /**
     * @brief Checks if the solution uses estimates.
     * 
     * @return true if the solution uses estimates, false in other case.  
     */
    constexpr static bool UsesEstimates()
    {
        return Estimate::value;
    }
};

/**
 * @brief Reduced Neighborhood for JSP.
 * 
 * @tparam Neighborhood.
 */
template <typename Neighborhood, typename RNG> class JSPReducedNeighborhood
{
  public:
    using ProblemType = typename Neighborhood::ProblemType;
    using TaskType = typename Neighborhood::TaskType;
    using MoveType = typename Neighborhood::MoveType;

  private:
    Neighborhood neighborhood;
    double rate;
    mutable RNG rng;

  public:
    /**
   * @brief Constructs a reduced neighborhood from the given neighborhood by randomly selecting
   * a percentage of the critical blocks according to their weights.
   * 
   * @param neighborhood neighborhood to reduce.
   * @param rate percentage of critical blocks to keep.
   * @param rng random number generator to be used to select the neighborhoods.
   */
    JSPReducedNeighborhood(Neighborhood& neighborhood, double rate, RNG& rng) : neighborhood{neighborhood}, rate{rate}, rng{rng} {}

    /**
     * @brief Inserts in a container the neighbors of a solution.
     * 
     * @tparam Iter type of the iterator to be used to insert the neighbors.
     * @tparam Solution type of the solution whose neighbors will be calculated.
     * @param dest iterator to be used to insert the neighbors.
     * @param solution solution whose neighbors will be calculated.
     * @return an iterator to the neighbor past the last neighbor inserted. 
     */
    template <typename Iter, typename Solution> Iter GetNeighbors(Iter dest, const Solution& solution) const
    {
        std::vector<JSPCriticalBlock<ProblemType>> critical_blocks;
        solution.GetCriticalBlocks(std::back_inserter(critical_blocks));

        std::vector<JSPCriticalBlock<ProblemType>> new_blocks;
        std::vector<JSPCriticalBlock<ProblemType>> remaining_blocks;

        // aggregate the quality of the elements to obtain the probability intervals
        std::vector<std::tuple<std::reference_wrapper<const JSPCriticalBlock<ProblemType>>, double, bool>> probability_intervals;
        auto first = critical_blocks.cbegin();
        auto last = critical_blocks.cend();
        probability_intervals.push_back(std::make_tuple(std::cref(*first), first->GetWeight(), true));
        ++first;
        while (first != last) {
            probability_intervals.push_back(std::make_tuple(std::cref(*first), std::get<1>(probability_intervals.back()) + first->GetWeight(), true));
            ++first;
        }

        // select the elements by generating random numbers in the previously calculated interval
        std::uniform_real_distribution<double> dis(0.0, std::get<1>(probability_intervals.back()));
        auto number_of_blocks = critical_blocks.size() * rate;
        auto current_selected_blocks = 0;
        while (current_selected_blocks < number_of_blocks) {
            auto selected =
                std::upper_bound(probability_intervals.begin(), probability_intervals.end(), dis(rng), [](const auto& b1, const auto& b2) {
                    return b1 < std::get<1>(b2);
                });
            if (std::get<2>(*selected)) {
                new_blocks.push_back(std::cref(std::get<0>(*selected)));
                current_selected_blocks++;
            }
        }

        return neighborhood.CalculateNeighbors(new_blocks.begin(), new_blocks.end(), dest, solution);
    }

    /**
     * @brief Checks if the solution uses estimates.
     * 
     * @return true if the solution uses estimates, false in other case.  
     */
    constexpr static bool UsesEstimates()
    {
        return Neighborhood::Estimate::value;
    }
};

/**
 * @brief Combined Neighborhood for JSP. Reuses critical blocks for all the neighborhoods.
 * 
 * @tparam Neighborhood type of the first neighborhood to be used.
 * @tparam Neighborhoods types of the additional neighborhoods to be used.
 */
template <typename Neighborhood, typename... Neighborhoods> class JSPCombinedNeighborhood
{
  public:
    using ProblemType = typename Neighborhood::ProblemType;
    using TaskType = typename Neighborhood::TaskType;
    using MoveType = typename Neighborhood::MoveType;

  private:
    std::tuple<Neighborhood, Neighborhoods...> neighborhoods;

  private:
    template <size_t I, typename Solution, typename InputIt, typename OutputIt>
    OutputIt CalculateNeighbors(InputIt first, InputIt last, OutputIt dest, const Solution& solution) const
    {
        if constexpr (I == sizeof...(Neighborhoods)) {
            return std::get<I>(neighborhoods).CalculateNeighbors(first, last, dest, solution);
        } else {
            *dest++ = std::get<I>(neighborhoods).CalculateNeighbors(first, last, dest, solution);
            return CalculateNeighbors<I + 1>(first, last, dest, solution);
        }
    }

  public:
    /**
     * @brief Constructs a combined neighborhood from the given neighborhoods.
     * 
     * @param neighborhood first neighborhood to be used.
     * @param neighborhoods additional neighborhoods to be used.
     */
    JSPCombinedNeighborhood(const Neighborhood& neighborhood, const Neighborhoods&... neighborhoods) : neighborhoods{neighborhood, neighborhoods...}
    {}

    /**
     * @brief Calculates the neighbors of a solution.
     * 
     * @tparam InputIt type of the iterator to be used to read the critical blocks.
     * @tparam OutputIt type of the iterator to be used to insert the neighbors.
     * @tparam Solution type of the solution.
     * @param first iterator pointing to the first critical block to be considered.
     * @param last iterator pointing to the critical block past the last critical block to be considered.
     * @param dest iterator to be used to insert the neighbors.
     * @param solution solution whose neighbors will be calculated.
     * @return an iterator to the neighbor past the last neighbor inserted.  
     */
    template <typename InputIt, typename OutputIt, typename Solution>
    OutputIt CalculateNeighbors(InputIt first, InputIt last, OutputIt dest, Solution solution) const
    {
        return CalculateNeighbors<0>(first, last, dest, solution);
    }

    /**
     * @brief Inserts in a container the neighbors of a solution.
     * 
     * @tparam Iter type of the iterator to be used to insert the neighbors.
     * @tparam Solution type of the solution whose neighbors will be calculated.
     * @param dest iterator to be used to insert the neighbors.
     * @param solution solution whose neighbors will be calculated.
     * @return an iterator to the neighbor past the last neighbor inserted. 
     */
    template <typename Iter, typename Solution> Iter GetNeighbors(Iter dest, const Solution& solution) const
    {
        std::unordered_set<JSPCriticalBlock<ProblemType>, std::hash<JSPCriticalBlock<ProblemType>>, std::equal_to<JSPCriticalBlock<ProblemType>>>
            critical_blocks;
        solution.GetCriticalBlocks(std::inserter(critical_blocks, critical_blocks.begin()));
        return CalculateNeighbors(critical_blocks.begin(), critical_blocks.end(), dest, solution);
    }
};
#endif /* JSPNEIGHBORHOODS_HPP_ */
