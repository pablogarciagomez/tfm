/**
 * @file jsp_readers.hpp
 * @author Pablo
 * @brief Readers for JSP instances.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef JSPREADERS_HPP_
#define JSPREADERS_HPP_

#include <iostream>
#include <string>
#include <utility>

#include <problems/jsp/jsp.hpp>

/**
 * @brief Reads a JSP instance in standard format.
 * 
 * @tparam Task type of the tasks.
 * @tparam Job type of the jobs.
 * @tparam Machine type of the machines.
 * @param is input stream. 
 * @return the problem read.
 */
template <typename Task, typename Job, typename Machine> JSP<Task, Job, Machine> read_standard(std::istream& is)
{
    JSP<Task, Job, Machine> jsp;
    unsigned int tasks;
    unsigned int jobs;
    unsigned int machines;
    unsigned int machine;
    typename Task::TimeType duration;
    is >> jobs;
    is >> machines;
    tasks = 1;
    for (unsigned int j = 1; j <= jobs; j++) {
        jsp.AddJob(j);
        for (unsigned int m = 0; m < machines; m++) {
            is >> machine;
            is >> duration;
            std::vector<std::pair<unsigned int, typename Task::TimeType>> durations;
            durations.push_back(std::make_pair(machine + 1, duration));
            jsp.AddMachine(machine + 1);
            jsp.AddTask(tasks++, j, durations.begin(), durations.end());
        }
    }
    return jsp;
}

/**
 * @brief Reads a JSP instance with due dates in standard format.
 * 
 * @tparam Task type of the tasks.
 * @tparam Job type of the jobs.
 * @tparam Machine type of the machines.
 * @param is input stream. 
 * @return the problem read.
 */
template <typename Task, typename Job, typename Machine> JSP<Task, Job, Machine> read_standard_due_dates(std::istream& is)
{
    JSP<Task, Job, Machine> jsp;
    unsigned int tasks;
    unsigned int jobs;
    unsigned int machines;
    unsigned int machine;
    typename Task::TimeType duration;
    typename Job::DateType due_date;
    typename Job::WeightType weight;
    is >> jobs;
    is >> machines;
    tasks = 1;
    for (unsigned int j = 1; j <= jobs; j++) {
        is >> due_date;
        is >> weight;
        jsp.AddJob(j, due_date, weight);
        for (unsigned int m = 0; m < machines; m++) {
            is >> machine;
            is >> duration;
            std::vector<std::pair<unsigned int, typename Task::TimeType>> durations;
            durations.push_back(std::make_pair(machine + 1, duration));
            jsp.AddMachine(machine + 1);
            jsp.AddTask(tasks++, j, durations.begin(), durations.end());
        }
    }
    return jsp;
}

/**
 * @brief Reads a FJSP instance in standard format.
 * 
 * @tparam Task type of the tasks.
 * @tparam Job type of the jobs.
 * @tparam Machine type of the machines.
 * @param is input stream. 
 * @return the problem read.
 */
template <typename Task, typename Job, typename Machine> JSP<Task, Job, Machine> read_flexible(std::istream& is)
{
    JSP<Task, Job, Machine> jsp;
    unsigned int jobs;
    unsigned int machines;
    unsigned int tasks;
    unsigned int tasks_in_job;
    typename Task::TimeType duration;
    is >> jobs;
    is >> machines;

    for (unsigned int j = 0; j < jobs; j++) {
        jsp.AddJob(j + 1);
    }

    for (unsigned int m = 0; m < machines; m++) {
        jsp.AddMachine(m + 1);
    }

    tasks = 1;
    for (unsigned int j = 0; j < jobs; j++) {
        is >> tasks_in_job;
        for (unsigned int t = 0; t < tasks_in_job; t++) {
            std::vector<std::pair<unsigned int, typename Task::TimeType>> durations;
            for (unsigned int m = 0; m < machines; m++) {
                is >> duration;
                if (duration > typename Task::TimeType{}) {
                    durations.push_back(std::make_pair(m + 1, duration));
                }
            }
            jsp.AddTask(tasks++, j + 1, durations.begin(), durations.end());
        }
    }
    return jsp;
}

/**
 * @brief Reads a FJSP with due dates instance in standard format.
 * 
 * @tparam Task type of the tasks.
 * @tparam Job type of the jobs.
 * @tparam Machine type of the machines.
 * @param is input stream. 
 * @return the problem read.
 */
template <typename Task, typename Job, typename Machine> JSP<Task, Job, Machine> read_flexible_due_dates(std::istream& is)
{
    JSP<Task, Job, Machine> jsp;
    unsigned int jobs;
    unsigned int machines;
    unsigned int tasks;
    unsigned int tasks_in_job;
    typename Task::TimeType duration;
    typename Job::DateType due_date;
    typename Job::WeightType weight;

    is >> jobs;
    is >> machines;

    for (unsigned int m = 0; m < machines; m++) {
        jsp.AddMachine(m + 1);
    }

    tasks = 1;
    for (unsigned int j = 0; j < jobs; j++) {
        is >> tasks_in_job;
        is >> due_date;
        is >> weight;
        jsp.AddJob(j + 1, due_date, weight);
        for (unsigned int t = 0; t < tasks_in_job; t++) {
            std::vector<std::pair<unsigned int, typename Task::TimeType>> durations;
            for (unsigned int m = 0; m < machines; m++) {
                is >> duration;
                if (duration > typename Task::TimeType{}) {
                    durations.push_back(std::make_pair(m + 1, duration));
                }
            }
            jsp.AddTask(tasks++, j + 1, durations.begin(), durations.end());
        }
    }
    return jsp;
}

#endif /* JSPREADERS_HPP_ */