/**
 * @file jsp_task.hpp
 * @author Pablo
 * @brief JSP Task.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef JSPTASK_HPP_
#define JSPTASK_HPP_

#include <string>
#include <unordered_map>
#include <utility>

#include <external/robin_hood.h>
#include <problems/jsp/jsp_machine.hpp>
#include <utils/container_utils.hpp>

/**
 * @brief Task of a JSP.
 * 
 * @tparam Time type of the time unit.
 * @tparam Job type of the job to which the task belongs.
 * @tparam Machine type of the machines in which the task will be executed.
 */
template <typename Time, typename Job, typename Machine> class JSPTask
{
  public:
    using TimeType = Time; // type of the time unit
    using JobType = Job; // type of the job to which the task belongs
    using MachineType = Machine; // type of the machine to which the task belongs
  private:
    unsigned int taskID; // identifier of the task
    std::reference_wrapper<const JobType> job; // the job to which the task belongs
    std::size_t position; // position of the task in the job
    robin_hood::unordered_map<std::reference_wrapper<const MachineType>, TimeType, std::hash<MachineType>, std::equal_to<MachineType>>
        durations; // duration of the task in each machine

  public:
    /**
     * @brief Construct a new JSPTask.
     * 
     * @tparam Iter type of the iterator to be used to read the execution time on each machine.
     * @param taskID identifier of the task.
     * @param job the job to which the task belongs.
     * @param position position of the task in the job.
     * @param first iterator pointing to the first execution time.
     * @param last iterator pointing to the execution time past the last execution time.
     */
    template <typename Iter>
    JSPTask(unsigned int taskID, const JobType& job, std::size_t position, Iter first, Iter last) :
        taskID{taskID},
        job{job},
        position{position},
        durations{first, last}
    {}

    /**
     * @brief Returns the identifier of the task.
     * 
     * @return the identifier of the task.
     */
    unsigned int GetTaskID() const
    {
        return taskID;
    }

    /**
     * @brief Returns the job to which the task belongs.
     * 
     * @return the job to which the task belongs.
     */
    const JobType& GetJob() const
    {
        return job;
    }

    /**
     * @brief Inserts in a container all the machines in which the task can be executed.
     * 
     * @tparam Iter type of the iterator to be used to insert the machines.
     * @param dest iterator to be used to insert the machines. 
     * @return an iterator to the machine past the last machine inserted. 
     */
    template <typename Iter> Iter GetMachines(Iter dest) const
    {
        return std::transform(durations.begin(), durations.end(), dest, [](const auto& pair) { return std::cref(pair.first); });
    }

    /**
     * @brief Returns the position of the task in the job.
     * 
     * @return the position of the task in the job.
     */
    std::size_t GetPosition() const
    {
        return position;
    }

    /**
     * @brief Returns the execution time of the task in the specified machine.
     * 
     * @return the execution time of the task in the specified machine.
     */
    TimeType GetDuration(const MachineType& machine) const
    {
        return durations.at(machine);
    }

    bool operator==(const JSPTask& other) const
    {
        return taskID == other.taskID;
    }

    bool operator!=(const JSPTask& other) const
    {
        return taskID != other.taskID;
    }

    friend std::ostream& operator<<(std::ostream& os, const JSPTask& task)
    {
        return os << "{" << task.taskID << "," << task.job << "," << task.position << "}";
    }
};

namespace std
{
    template <typename Time, typename Job, typename Machine> struct hash<JSPTask<Time, Job, Machine>>
    {
        size_t operator()(const JSPTask<Time, Job, Machine>& k) const
        {
            return k.GetTaskID();
        }
    };
}

#endif /* JSPTASK_HPP_ */
