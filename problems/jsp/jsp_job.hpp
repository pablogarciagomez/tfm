/**
 * @file jsp_job.hpp
 * @author Pablo
 * @brief JSP Job.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef JSPJOB_HPP_
#define JSPJOB_HPP_

#include <string>
#include <utility>

/**
 * @brief Job of a JSP.
 * 
 * @tparam Date type of the date unit.
 * @tparam Weight type of the weight unit.
 */
template <typename Date, typename Weight> class JSPJob
{
  public:
    using DateType = Date; // type of the date unit
    using WeightType = Date; // type of the weight unit
  private:
    unsigned int jobID; // identifier of the job
    DateType due_date; // due date of the job
    WeightType weight; // weight (importance) of the job

  public:
    /**
     * @brief Constructs a new JSPJob.
     * 
     * @param jobID identifier of the job.   
     * @param due_date due date of the job.
     * @param weight weight (importance) of the job.
     */
    JSPJob(unsigned int jobID, DateType due_date = DateType(), WeightType weight = 1) : jobID{jobID}, due_date{due_date}, weight{weight} {}

    /**
     * @brief Returns the identifier of the job.
     * 
     * @return the identifier of the job.
     */
    unsigned int GetJobID() const
    {
        return jobID;
    }

    /**
     * @brief Returns the due date of the job.
     * 
     * @return the due date of the job.
     */
    DateType GetDueDate() const
    {
        return due_date;
    }

    /**
     * @brief Returns the weight of the job.
     * 
     * @return the weight of the job.
     */
    WeightType GetWeight() const
    {
        return weight;
    }

    bool operator==(const JSPJob& other) const
    {
        return jobID == other.jobID;
    }

    bool operator!=(const JSPJob& other) const
    {
        return jobID != other.jobID;
    }

    friend std::ostream& operator<<(std::ostream& os, const JSPJob& job)
    {
        return os << "{" << job.jobID << "," << job.due_date << "," << job.weight << "}";
    }
};

namespace std
{
    template <typename Date, typename Weight> struct hash<JSPJob<Date, Weight>>
    {
        size_t operator()(const JSPJob<Date, Weight>& k) const
        {
            return k.GetJobID();
        }
    };
}

#endif /* JSPJOB_HPP_ */
