/**
 * @file jsp_critical_block.hpp
 * @author Pablo
 * @brief JSP Critical Block.
 * @version 1.0
 * @date 25-06-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef JSPCRITICALBLOCK_HPP_
#define JSPCRITICALBLOCK_HPP_

#include <list>

#include <utils/container_utils.hpp>

/**
 * @brief JSP critical block.
 * 
 * @tparam Problem type of the problem.
 */
template <typename Problem> class JSPCriticalBlock
{
  public:
    friend class std::hash<JSPCriticalBlock>;
    using ProblemType = Problem;
    using TaskType = typename ProblemType::TaskType;

  private:
    std::list<std::reference_wrapper<const TaskType>> tasks;
    double weight;

  public:
    /**
     * @brief Constructs a new JSPCriticalBlock.
     * 
     * @param weight weight of the critical block.
     */
    JSPCriticalBlock(double weight) : weight(weight) {}

    /**
     * @brief Inserts a task in the block.
     * 
     * @param task task to be inserted.
     */
    void InsertTask(const TaskType& task)
    {
        tasks.push_back(std::cref(task));
    }

    /**
     * @brief Checks if the block has no elements.
     * 
     * @return true if the block has no elements, false in other case.
     */
    bool Empty() const
    {
        return tasks.empty();
    }

    /**
     * @brief Returns the number of tasks in the critical block.
     * 
     * @return the number of tasks in the critical block.
     */
    std::size_t GetNumberOfTasks() const
    {
        return tasks.size();
    }

    /**
     * @brief Returns the weight.
     * 
     * @return the weight.
     */
    double GetWeight() const
    {
        return weight;
    }

    template <typename Iter> Iter GetTasks(Iter dest) const
    {
        return std::copy(tasks.rbegin(), tasks.rend(), dest);
    }

    bool operator==(const JSPCriticalBlock& other) const
    {
        return tasks.size() == other.tasks.size() && tasks.front().get() == other.tasks.front().get() &&
               tasks.back().get() == other.tasks.back().get();
    }

    bool operator!=(const JSPCriticalBlock& other) const
    {
        return tasks.size() != other.tasks.size() || tasks.front().get() != other.tasks.front().get() ||
               tasks.back().get() != other.tasks.back().get();
    }
};

namespace std
{
    template <typename Problem> struct hash<JSPCriticalBlock<Problem>>
    {
        size_t operator()(const JSPCriticalBlock<Problem>& k) const
        {
            return hash_combine(k.tasks.front().get(), k.tasks.back().get(), k.tasks.size());
        }
    };
}

#endif /* JSPCRITICALBLOCK_HPP_ */
