import glob
import os
import re

factors = [1.0, 1.1, 1.2]


def avg(lst):
    return sum(lst) / len(lst)


for filename in glob.iglob('./**/*.txt', recursive=True):
    lines = [line.rstrip('\n') for line in open(filename)]
    n, m = lines[0].split()
    weights = [4]*int(int(n)*0.2) + [2]*int(int(n)*0.6) + [1]*int(int(n)*0.2)
    for factor in factors:
        f = open(filename[:-4]+'_' +
                 str(factor).replace('.', '')+filename[-4:], 'w+')
        f.write(n+' '+m+'\n')

        current_line = 1
        for j in range(int(n)):
            number_of_tasks = lines[current_line]
            tasks = ""
            current_line += 1
            due_date = 0
            for t in range(int(number_of_tasks)):
                tasks += lines[current_line]+"\n"
                due_date += min(list([(int(re.split(r'\(|,|\)', e)[0])+2*int(re.split(r'\(|,|\)', e)[1])+int(re.split(r'\(|,|\)', e)[
                                2]))/4 for e in re.split(r'\(|\)', lines[current_line]) if e != '' and e != ' ' and re.split(r'\(|,|\)', e)[1] != '-1']))
                current_line += 1
            f.write(str(number_of_tasks)+" "+str(round(due_date *
                                                       factor, 3))+" "+str(weights[j])+"\n"+tasks)

        f.close()
    os.remove(filename)
