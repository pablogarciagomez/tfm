import glob
import os
import sys
import re
import statistics

real_execution_times = list()
cpu_execution_times = list()
values = list()

for filename in sys.argv[2:]:
    f = open(filename)
    trace = f.read()
    f.close()
    real_execution_times.append(
        float(re.search('Real Time = (.*)', trace).group(1)))
    cpu_execution_times.append(
        float(re.search('CPU Time = (.*)', trace).group(1)))
    values.append(
        float(re.search('Expected Total Weighted Tardiness = (.*)', trace).group(1)))

f = open(sys.argv[1], 'w+')

f.write('Best = '+str(min(values))+'\n')
f.write('Average = '+str(statistics.mean(values))+'\n')
f.write('Standard deviation = '+str(statistics.stdev(values))+'\n')
f.write('Average real execution time = ' +
        str(statistics.mean(real_execution_times)/1000000)+'\n')
f.write('Average CPU execution time = ' +
        str(statistics.mean(cpu_execution_times)/1000000)+'\n')

f.close()
