A C++ header-only library for solving the job shop scheduling problem. It has support for flexibility and fuzzy processing times.


## Usage

This is a header-only library so you only have to include the headers you need. The library is divided in two parts, the [problems](problems/) and the [metaheuristics](metaheuristics/). The directory [scripts](scripts/) contains useful Python scripts to process the input and output files. 

The file [main.cpp](main.cpp) is an example of a program that solves the fuzzy flexible job shop scheduling problem minimizing the total weighted tardiness.

The complete documentation of the code can be found in the directory [doc](doc/).


## Author

Pablo García Gómez
