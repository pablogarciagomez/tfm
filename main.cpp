#include <chrono>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <functional>
#include <iostream>
#include <limits>
#include <random>

#include <metaheuristics/evolutionary_algorithm/crossover_operators.hpp>
#include <metaheuristics/evolutionary_algorithm/dual_evolutionary_algorithm.hpp>
#include <metaheuristics/evolutionary_algorithm/dual_memetic_algorithm.hpp>
#include <metaheuristics/evolutionary_algorithm/evolutionary_algorithm.hpp>
#include <metaheuristics/evolutionary_algorithm/memetic_algorithm.hpp>
#include <metaheuristics/evolutionary_algorithm/mutation_operators.hpp>
#include <metaheuristics/evolutionary_algorithm/replacement_operators.hpp>
#include <metaheuristics/evolutionary_algorithm/selection_operators.hpp>
#include <metaheuristics/tabu_search/tabu_search_fixed_length.hpp>
#include <metaheuristics/tabu_search/tabu_search_variable_length.hpp>
#include <metaheuristics/utils/evolutionary_algorithm_logger.hpp>
#include <metaheuristics/utils/local_search_logger.hpp>
#include <problems/jsp/jsp_generation_operators.hpp>
#include <problems/jsp/jsp_job.hpp>
#include <problems/jsp/jsp_machine.hpp>
#include <problems/jsp/jsp_neighborhoods.hpp>
#include <problems/jsp/jsp_readers.hpp>
#include <problems/jsp/jsp_regular_measures_minimization_solution.hpp>
#include <problems/jsp/jsp_task.hpp>

template <typename RealNumber> static bool AlmostEqual(RealNumber a, RealNumber b, RealNumber tolerance = std::numeric_limits<RealNumber>::epsilon())
{
    RealNumber d = std::fabs(a - b);
    if (d <= tolerance) {
        return true;
    }
    if (d < std::fmax(std::fabs(a), std::fabs(b)) * tolerance) {
        return true;
    }
    return false;
}

template <typename Problem, typename Solution>
std::tuple<Solution, EvolutionaryAlgorithmLogger<std::true_type>, LocalSearchLogger<std::false_type>> MemeticAlgorithm(Problem& problem)
{
    std::random_device rd{};
    std::mt19937 rng(rd());
    JSPRandomPopulationGenerator generation_operator{};
    DualGOX crossover_op{};
    PairSelection selection_op{};
    None mutation_op{};
    Tournament replacement_op{};
    DualGeneEncoder<GT> encoder_decoder{};
    TabuSearchVariableLength<std::true_type> local_search{};
    CET<Problem, JSPMove, std::true_type> cet{};
    MachineReassignation<Problem, JSPMove, std::true_type> random{};
    JSPCombinedNeighborhood combined{cet, random};

    EvolutionaryAlgorithmLogger<std::true_type> evolutionary_logger;
    LocalSearchLogger<std::false_type> local_logger;

    auto solution = MemeticAlgorithm::FindSolution<Solution>(
        evolutionary_logger,
        local_logger,
        problem,
        encoder_decoder,
        generation_operator,
        (problem.GetNumberOfTasks() / problem.GetNumberOfMachines()) * problem.GetNumberOfJobs(),
        selection_op,
        crossover_op,
        1.0,
        mutation_op,
        0.0,
        replacement_op,
        false,
        [&problem](auto, auto no_improving_generations, auto average_quality, auto best_solution_quality) {
            return no_improving_generations > (problem.GetNumberOfJobs() + problem.GetNumberOfMachines()) ||
                   AlmostEqual(best_solution_quality, average_quality);
        },
        rng,
        local_search,
        1.0,
        problem.GetNumberOfTasks(),
        2 * problem.GetNumberOfTasks(),
        [&problem](auto, auto no_improving_iterations) { return no_improving_iterations > problem.GetNumberOfTasks(); },
        combined);

    return std::tie(solution, evolutionary_logger, local_logger);
}

int main(int argc, char** argv)
{
    if (argc < 3) {
        throw std::invalid_argument("Missing arguments");
    }

    using JobType = JSPJob<double, double>;
    using MachineType = JSPMachine;
    using TaskType = JSPTask<TriangularFuzzyNumber<double>, JobType, MachineType>;
    using ProblemType = JSP<TaskType, JobType, MachineType>;
    using SolutionType = JSPRegularMeasuresMinimizationSolution<ProblemType, std::integral_constant<int, 1>, std::true_type>;

    std::ifstream instance(argv[1]);
    std::ofstream trace(argv[2]);
    auto problem = read_flexible_due_dates<TaskType, JobType, MachineType>(instance);

    std::clock_t cpu_start = std::clock();
    auto wall_start = std::chrono::high_resolution_clock::now();
    auto [solution, evolutionary_logger, local_search_logger] = MemeticAlgorithm<ProblemType, SolutionType>(problem);
    auto wall_end = std::chrono::high_resolution_clock::now();
    std::clock_t cpu_end = std::clock();

    trace << "Real Time = " << std::chrono::duration_cast<std::chrono::microseconds>(wall_end - wall_start).count() << std::endl;
    trace << "CPU Time = " << cpu_end - cpu_start << std::endl;
    trace << "Total Weighted Tardiness = " << solution.GetTotalWeightedTardiness() << std::endl;
    trace << "Expected Total Weighted Tardiness = " << solution.GetTotalWeightedTardiness().ExpectedValue() << std::endl;
    trace << "TRACE" << std::endl;
    trace << evolutionary_logger;
    trace << "SOLUTION" << std::endl;
    trace << solution << std::endl;

    return 0;
}
